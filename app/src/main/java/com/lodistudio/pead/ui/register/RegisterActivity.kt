package com.lodistudio.pead.ui.register

import android.os.Bundle
import android.view.MenuItem
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.lodistudio.pead.R
import com.lodistudio.pead.data.local.entity.Role
import com.lodistudio.pead.data.local.entity.User
import com.lodistudio.pead.data.repositories.RoleRepository
import com.lodistudio.pead.data.repositories.UserRepository
import com.lodistudio.pead.util.*
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : BaseActivity() {
    lateinit var userRepo: UserRepository
    lateinit var roleRepo: RoleRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        userRepo = UserRepository.getInstance(this)!!
        roleRepo = RoleRepository.getInstance(this)!!

        setupListeners()
        setupToolbar()
        populateRoles()
    }

    private fun setupListeners(){
        btn_save.setOnClickListener {
            createNewUser()
        }
    }
    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)

        toolbar.title = getString(R.string.activity_register_toolbar_title)
    }
    private fun populateRoles(){
        val roleAdapter = ArrayAdapter<AppConstants.Roles>(this,android.R.layout.simple_list_item_1, AppConstants.Roles.values())
        role.adapter = roleAdapter
    }

    private fun createNewUser(){
        if (!validateFields()) return
        showLoading()

        userRepo.getUserByEmail(email.text.toString()).observeOnce(this, Observer{user ->
            hideLoading()
                if (user == null){
                    var newUser = User(
                        name = name.text.toString(),
                        phone = phone.rawText?.toLongOrNull(),
                        email = email.text.toString(),
                        password = password.text.toString(),
                        rg = rg.rawText?.toLongOrNull(),
                        client_cnpj = cnpj.rawText?.toLongOrNull(),
                        role = if(role.selectedItem != null) (role.selectedItem as AppConstants.Roles).publicName else null,
                        certificate_number = certificate.text.toString()
                    )

                    userRepo.insert(newUser)

                    toast("Usuário cadastrado com sucesso!")
                    log("Usuário cadastrado com sucesso! Model: ${newUser.toString()}")
                    finishAfterTransition()
                }else{
                    log("Usuário '${user.name}' foi encontrado através do e-mail. ")
                    email.error = ValidationErrors.emailAlreadyTakenError
                    email.requestFocus()
                }
        })
    }
    private fun validateFields(): Boolean{

        if(name.text.toString().isEmpty())
            name.error = ValidationErrors.requiredFieldError
        if(phone.rawText.isNullOrEmpty())
            phone.error = ValidationErrors.requiredFieldError
        if(email.text.toString().isEmpty())
            email.error = ValidationErrors.requiredFieldError
        else if(!ValidationUtils.isValidEmail(email.text.toString()))
            email.error = ValidationErrors.emailInvalidError
        if(password.text.toString().isEmpty())
            password.error = ValidationErrors.requiredFieldError
        else if(!ValidationUtils.isValidPassword(password.text.toString()))
            password.error = ValidationErrors.passwordInvalidLengthError
        if(rg.rawText.isNullOrEmpty())
            rg.error = ValidationErrors.requiredFieldError
        if(cnpj.rawText.isNullOrEmpty())
            cnpj.error = ValidationErrors.requiredFieldError
        if(certificate.text.toString().isEmpty())
            certificate.error = ValidationErrors.requiredFieldError

       return !hasErrors()
    }
    private fun hasErrors(): Boolean{

        if(!name.error.isNullOrEmpty()) return true
        if(!phone.error.isNullOrEmpty()) return true
        if(!email.error.isNullOrEmpty()) return true
        if(!password.error.isNullOrEmpty()) return true
        if(!rg.error.isNullOrEmpty()) rg.error = return true
        if(!cnpj.error.isNullOrEmpty()) cnpj.error = return true
        if(!certificate.error.isNullOrEmpty()) return true

        return false
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finishAfterTransition()
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onBackPressed() {
        super.onBackPressed()
        finishAfterTransition()
    }

}
