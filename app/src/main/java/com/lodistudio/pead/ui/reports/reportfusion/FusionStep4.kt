package com.lodistudio.pead.ui.reports.reportfusion

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.gms.ads.AdRequest
import com.lodistudio.pead.R
import com.lodistudio.pead.ui.newreport.NewReport
import com.lodistudio.pead.util.AppConstants
import kotlinx.android.synthetic.main.fragment_fusion_step4.*
import kotlinx.android.synthetic.main.fragment_fusion_step4_tab1.view.*
import kotlinx.android.synthetic.main.fragment_fusion_step4_tab2.view.*
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class FusionStep4 : BaseStep() {
    companion object{
        const val TAB1 = "tab1"
        const val TAB2 = "tab2"
    }

    val REQUEST_ID_MULTIPLE_PERMISSIONS = 10
    val CAMERA = 12
    val RESULT_CANCELED = 14
    var evidence_click = ""

    var field_material1_manufacturer = ""
    var field_material2_manufacturer = ""
    var field_material1_external_diameter = 0
    var field_material2_external_diameter = 0
    var field_material1_resin_type = ""
    var field_material2_resin_type = ""
    var field_material1_sdr = 0.0
    var field_material2_sdr = 0.0
    var field_material1_pn = 0.0
    var field_material2_pn = 0.0
    var field_material1_lote_number = ""
    var field_material2_lote_number = ""
    var field_material1_lote_number_evidence: Uri? = null
    var field_material2_lote_number_evidence: Uri? = null
    var field_material1_rule = ""
    var field_material2_rule = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fusion_step4, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent = context as NewReport

        setupListeners()
        setupAds()
        setupTabs()
    }

    fun setupListeners(){
        //TAB 1
        tab1.field6_1_evidence.setOnClickListener {
            evidence_click = TAB1
            if(checkPermissions()) choosePhotoFromGallery()
        }


        //TAB 2
        tab2.field6_2_evidence.setOnClickListener {
            evidence_click = TAB2
            if(checkPermissions()) choosePhotoFromGallery()
        }

        tab2.btn_copy.setOnClickListener { 
            copyFromTab1()
        }

    }
    
    fun copyFromTab1(){
        tab2.field1_2.text = tab1.field1_1.text
        tab2.field2_2.text = tab1.field2_1.text
        tab2.field3_2.setSelection(tab1.field3_1.selectedItemPosition)
        tab2.field4_2.setSelection(tab1.field4_1.selectedItemPosition)
        tab2.field5_2.text = tab1.field5_1.text
        tab2.field6_2.text = tab1.field6_1.text
        tab2.field8_2.setSelection(tab1.field8_1.selectedItemPosition)
        
        field_material2_manufacturer = field_material1_manufacturer
        field_material2_external_diameter = field_material1_external_diameter
        field_material2_resin_type = field_material1_resin_type
        field_material2_sdr = field_material1_sdr
        field_material2_pn = field_material1_pn
        field_material2_lote_number = field_material1_lote_number
        field_material2_lote_number_evidence = if(field_material1_lote_number_evidence != null ) field_material1_lote_number_evidence else null
        field_material2_rule = field_material1_rule

        if(field_material2_lote_number_evidence != null)
            tab2.field6_2_evidence.setImageDrawable(ContextCompat.getDrawable(parent, R.drawable.ic_evidence_ok))

    }

    fun setupAds(){
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }
    fun setupTabs(){
        tabHost.setup()

        var spec1 = tabHost.newTabSpec(parent.reportFusion.material_1_type)
        var spec2 = tabHost.newTabSpec(parent.reportFusion.material_2_type)

        spec1.setIndicator(parent.reportFusion.material_1_type)
        spec1.setContent(R.id.tab1)

        spec2.setIndicator(parent.reportFusion.material_2_type)
        spec2.setContent(R.id.tab2)

        tabHost.addTab(spec1)
        tabHost.addTab(spec2)

        populateResintypes()
        populateSDRs()
        populateRules()
    }

    private fun populateResintypes(){
        //POPULATE TAB 1
        val resinTypeAdapter = ArrayAdapter<AppConstants.ResinType>(parent,android.R.layout.simple_list_item_1, AppConstants.ResinType.values())
        tab1.field3_1.adapter = resinTypeAdapter

        tab1.field3_1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {return}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position > 0) {
                    if(tab1.field4_1.selectedItem != null && tab1.field4_1.selectedItemId > 0){
                        var sdrSelected = (tab1.field4_1.selectedItem as AppConstants.SDR)
                        var formatedPN = NumberFormat.getInstance().format(AppConstants.SDR.getPN(AppConstants.ResinType.values()[position], sdrSelected.value))
                        tab1.field5_1.text = "$formatedPN"
                    }
                }else{
                    tab1.field5_1.text = ""
                }
            }
        }

        //POPULATE TAB 2
        val resinTypeAdapter2 = ArrayAdapter<AppConstants.ResinType>(parent,android.R.layout.simple_list_item_1, AppConstants.ResinType.values())
        tab2.field3_2.adapter = resinTypeAdapter2

        tab2.field3_2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {return}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position > 0) {
                    if(tab2.field4_2.selectedItem != null && tab2.field4_2.selectedItemId > 0){
                        var sdrSelected = (tab2.field4_2.selectedItem as AppConstants.SDR)
                        var formatedPN = NumberFormat.getInstance().format(AppConstants.SDR.getPN(AppConstants.ResinType.values()[position], sdrSelected.value))
                        tab2.field5_2.text = "$formatedPN"
                    }
                }else{
                    tab2.field5_2.text = ""
                }
            }
        }
    }
    private fun populateSDRs(){
        //POPULATE TAB 1
        val sdrAdapter = ArrayAdapter<AppConstants.SDR>(parent,android.R.layout.simple_list_item_1, AppConstants.SDR.values())
        tab1.field4_1.adapter = sdrAdapter

        tab1.field4_1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {return}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position > 0) {
                    if(tab1.field3_1.selectedItem != null && tab1.field3_1.selectedItemId > 0){
                        var resinSelected = (tab1.field3_1.selectedItem as AppConstants.ResinType)
                        var formatedPN = NumberFormat.getInstance().format(AppConstants.SDR.getPN(resinSelected, AppConstants.SDR.values()[position].value))
                        tab1.field5_1.text = "$formatedPN"
                    }
                }else{
                    tab1.field5_1.text = ""
                }
            }
        }

        //POPULATE TAB 2
        val sdrAdapter2 = ArrayAdapter<AppConstants.SDR>(parent,android.R.layout.simple_list_item_1, AppConstants.SDR.values())
        tab2.field4_2.adapter = sdrAdapter2

        tab2.field4_2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {return}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position > 0) {
                    if(tab2.field3_2.selectedItem != null && tab2.field3_2.selectedItemId > 0){
                        var resinSelected = (tab2.field3_2.selectedItem as AppConstants.ResinType)
                        var formatedPN = NumberFormat.getInstance().format(AppConstants.SDR.getPN(resinSelected, AppConstants.SDR.values()[position].value))
                        tab2.field5_2.text = "$formatedPN"
                    }
                }else{
                    tab2.field5_2.text = ""
                }
            }
        }
    }
    private fun populateRules(){
        val rulesAdapter = ArrayAdapter<AppConstants.MaterialRules>(parent,android.R.layout.simple_list_item_1, AppConstants.MaterialRules.values())
        tab1.field8_1.adapter = rulesAdapter
        tab2.field8_2.adapter = rulesAdapter
    }

    private fun checkPermissions(): Boolean {
        val camerapermission = ContextCompat.checkSelfPermission(parent, Manifest.permission.CAMERA)
        val writepermission = ContextCompat.checkSelfPermission(parent, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val readpermission = ContextCompat.checkSelfPermission(parent, Manifest.permission.READ_EXTERNAL_STORAGE)

        val listPermissionsNeeded = ArrayList<String>()

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (readpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }

        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
            return false
        }
        return true
    }
    private fun choosePhotoFromGallery() {
        var cameraIntent =  Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        when(evidence_click){
            TAB1->{
                field_material1_lote_number_evidence = FileProvider.getUriForFile(parent, parent.packageName + ".fileprovider", getFile()!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, field_material1_lote_number_evidence)
            }
            TAB2->{
                field_material2_lote_number_evidence = FileProvider.getUriForFile(parent, parent.packageName + ".fileprovider", getFile()!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, field_material2_lote_number_evidence)
            }
        }

        if(cameraIntent.resolveActivity(parent.packageManager)!=null)
        {
            this.startActivityForResult(cameraIntent, CAMERA)
        }
    }
    private fun confirmEvidence(){
        when(evidence_click){
            TAB1->{
                evidence_click = ""
                tab1.field6_1_evidence.setImageResource(R.drawable.ic_evidence_ok)
                tab1.field6_1_error.visibility = View.GONE
                tab1.field6_1_evidence.invalidate()
            }
            TAB2->{
                evidence_click = ""
                tab2.field6_2_evidence.setImageResource(R.drawable.ic_evidence_ok)
                tab2.field6_2_error.visibility = View.GONE
                tab2.field6_2_evidence.invalidate()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            return
        }
        if (requestCode == CAMERA && resultCode == RESULT_OK) {
            confirmEvidence()
        }
    }

    private fun clearErrors(){
        tab1.field1_1_error.visibility = View.GONE
        tab1.field2_1_error.visibility = View.GONE
        tab1.field3_1_error.visibility = View.GONE
        tab1.field4_1_error.visibility = View.GONE
        tab1.field6_1_error.visibility = View.GONE
        tab1.field8_1_error.visibility = View.GONE
        tab2.field1_2_error.visibility = View.GONE
        tab2.field2_2_error.visibility = View.GONE
        tab2.field3_2_error.visibility = View.GONE
        tab2.field4_2_error.visibility = View.GONE
        tab2.field6_2_error.visibility = View.GONE
        tab2.field8_2_error.visibility = View.GONE
    }

    private fun validateFieldsTab1(): Boolean{
        if(tab1.field1_1.text.isNullOrEmpty()){
            tab1.field1_1_error.text = "O fabricante do ${parent.reportFusion.material_1_type} é obrigatório"
            tab1.field1_1_error.visibility = View.VISIBLE
        }

        if (tab1.field2_1.text.isNullOrEmpty()){
            tab1.field2_1_error.text = "O diametro externo do ${parent.reportFusion.material_1_type} é obrigatório"
            tab1.field2_1_error.visibility = View.VISIBLE
        }

        val f3selected = tab1.field3_1.selectedItem as AppConstants.ResinType
        if(f3selected != null && f3selected == AppConstants.ResinType.EMPTY){
            tab1.field3_1_error.text = "O tipo de resina do ${parent.reportFusion.material_1_type} é obrigatório"
            tab1.field3_1_error.visibility = View.VISIBLE
        }

        val f4selected = tab1.field4_1.selectedItem as AppConstants.SDR
        if(f4selected != null && f4selected == AppConstants.SDR.EMPTY){
            tab1.field4_1_error.text = "O SDR do ${parent.reportFusion.material_1_type} é obrigatório"
            tab1.field4_1_error.visibility = View.VISIBLE
        }

        if (tab1.field6_1.text.isNullOrEmpty()){
            tab1.field6_1_error.text = "O nº do lote do ${parent.reportFusion.material_1_type} é obrigatório"
            tab1.field6_1_error.visibility = View.VISIBLE
        }else if (field_material1_lote_number_evidence == null){
            tab1.field6_1_error.text = "Você precisa evidenciar o número do lote antes de continuar"
            tab1.field6_1_error.visibility = View.VISIBLE
        }

        if( tab1.field1_1_error.visibility == View.VISIBLE ||
            tab1.field2_1_error.visibility == View.VISIBLE ||
            tab1.field3_1_error.visibility == View.VISIBLE ||
            tab1.field4_1_error.visibility == View.VISIBLE ||
            tab1.field6_1_error.visibility == View.VISIBLE ||
            tab1.field8_1_error.visibility == View.VISIBLE)
                return false
        
        field_material1_manufacturer = tab1.field1_1.text.toString()
        field_material1_external_diameter = tab1.field2_1.text.toString().toIntOrNull() ?: 0
        val resinSelected = (tab1.field3_1.selectedItem as AppConstants.ResinType).publicName
        field_material1_resin_type = resinSelected
        val sdrSelected = (tab1.field4_1.selectedItem as AppConstants.SDR).value
        field_material1_sdr = sdrSelected
        field_material1_pn = tab1.field5_1.text.toString().toDoubleOrNull() ?: 0.0
        field_material1_lote_number = tab1.field6_1.text.toString()
        val ruleSelected = (tab1.field8_1.selectedItem as AppConstants.MaterialRules).publicName
        field_material1_rule = ruleSelected
        
        return true
    }

    private fun validateFieldsTab2(): Boolean{
        if(tab2.field1_2.text.isNullOrEmpty()){
            tab2.field1_2_error.text = "O fabricante do ${parent.reportFusion.material_2_type} é obrigatório"
            tab2.field1_2_error.visibility = View.VISIBLE
        }

        if (tab2.field2_2.text.isNullOrEmpty()){
            tab2.field2_2_error.text = "O diametro externo do ${parent.reportFusion.material_2_type} é obrigatório"
            tab2.field2_2_error.visibility = View.VISIBLE
        }

        val f3selected = tab2.field3_2.selectedItem as AppConstants.ResinType
        if(f3selected != null && f3selected == AppConstants.ResinType.EMPTY){
            tab2.field3_2_error.text = "O tipo de resina do ${parent.reportFusion.material_2_type} é obrigatório"
            tab2.field3_2_error.visibility = View.VISIBLE
        }

        val f4selected = tab2.field4_2.selectedItem as AppConstants.SDR
        if(f4selected != null && f4selected == AppConstants.SDR.EMPTY){
            tab2.field4_2_error.text = "O SDR do ${parent.reportFusion.material_2_type} é obrigatório"
            tab2.field4_2_error.visibility = View.VISIBLE
        }

        if (tab2.field6_2.text.isNullOrEmpty()){
            tab2.field6_2_error.text = "O nº do lote do ${parent.reportFusion.material_2_type} é obrigatório"
            tab2.field6_2_error.visibility = View.VISIBLE
        }else if (field_material1_lote_number_evidence == null){
            tab2.field6_2_error.text = "Você precisa evidenciar o número do lote antes de continuar"
            tab2.field6_2_error.visibility = View.VISIBLE
        }

        if( tab2.field1_2_error.visibility == View.VISIBLE ||
            tab2.field2_2_error.visibility == View.VISIBLE ||
            tab2.field3_2_error.visibility == View.VISIBLE ||
            tab2.field4_2_error.visibility == View.VISIBLE ||
            tab2.field6_2_error.visibility == View.VISIBLE ||
            tab2.field8_2_error.visibility == View.VISIBLE)
            return false

        field_material2_manufacturer = tab2.field1_2.text.toString()
        field_material2_external_diameter = tab2.field2_2.text.toString().toIntOrNull() ?: 0
        val resinSelected = (tab2.field3_2.selectedItem as AppConstants.ResinType).publicName
        field_material2_resin_type = resinSelected
        val sdrSelected = (tab2.field4_2.selectedItem as AppConstants.SDR).value
        field_material2_sdr = sdrSelected
        field_material2_pn = tab2.field5_2.text.toString().toDoubleOrNull() ?: 0.0
        field_material2_lote_number = tab2.field6_2.text.toString()
        val ruleSelected = (tab2.field8_2.selectedItem as AppConstants.MaterialRules).publicName
        field_material2_rule = ruleSelected
        
        return true
    }

    override fun validateFields(): Boolean{
        clearErrors()

        if(!validateFieldsTab1()){
            tabHost.currentTab = 0
            return false
        }
        if (!validateFieldsTab2()){
            tabHost.currentTab = 1
            return false
        }

        return true
    }
    override fun saveFields() {
        try {
            parent.reportFusion.material_1_manufacturer = field_material1_manufacturer
            parent.reportFusion.material_1_external_diameter = field_material1_external_diameter
            parent.reportFusion.material_1_resin_type = field_material1_resin_type
            parent.reportFusion.material_1_sdr = field_material1_sdr
            parent.reportFusion.material_1_pn = field_material1_pn
            parent.reportFusion.material_1_lote_number = field_material1_lote_number
            parent.reportFusion.material_1_lote_number_evidence = field_material1_lote_number_evidence.toString()
            parent.reportFusion.material_1_rule = field_material1_rule

            parent.reportFusion.material_2_manufacturer = field_material2_manufacturer
            parent.reportFusion.material_2_external_diameter = field_material2_external_diameter
            parent.reportFusion.material_2_resin_type = field_material2_resin_type
            parent.reportFusion.material_2_sdr = field_material2_sdr
            parent.reportFusion.material_2_pn = field_material2_pn
            parent.reportFusion.material_2_lote_number = field_material2_lote_number
            parent.reportFusion.material_2_lote_number_evidence = field_material2_lote_number_evidence.toString()
            parent.reportFusion.material_2_rule = field_material2_rule
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() SUCCESS!")
        }catch (ex: Exception){
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() ERROR: ${ex.message}")
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {

                val perms = HashMap<String, Int>()
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.READ_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED

                if (grantResults.size > 0) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    if (perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.READ_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    ) {
                        choosePhotoFromGallery()
                    }
                }
            }
        }

    }
}
