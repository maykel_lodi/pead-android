package com.lodistudio.pead.ui.reports.reportfusion

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.lodistudio.pead.R
import com.lodistudio.pead.data.repositories.WeatherRepository
import com.lodistudio.pead.ui.newreport.NewReport
import com.lodistudio.pead.util.AppConstants
import com.lodistudio.pead.util.DialogUtils
import com.lodistudio.pead.util.Session
import com.lodistudio.pead.util.locationUtils.EasyLocationUtility
import com.lodistudio.pead.util.locationUtils.LocationRequestCallback
import com.lodistudio.pead.util.observeOnce
import kotlinx.android.synthetic.main.fragment_fusion_step1.*
import kotlinx.android.synthetic.main.progressbar_basic.view.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.io.IOException
import java.lang.Exception
import java.util.*


class FusionStep1 : BaseStep() {
    lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var weatherRepo: WeatherRepository

    var field_dt_start: DateTime? = null
    var field_description = ""
    var field_climatic_description = ""
    var field_climatic_humidity = 0.0
    var field_climatic_temperature = 0.0
    var field_user_latitude = 0.0
    var field_user_longitude = 0.0

    companion object{
        const val PERMISSION_CALLBACK = 10
        const val loading_state_local = "Buscando dados de localização..."
        const val loading_state_climatic = "Buscando dados de clima..."
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fusion_step1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent = context as NewReport
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
        weatherRepo = WeatherRepository.getInstance(parent)!!

        setupAds()
        setupPage()
        setupListeners()
    }

    fun setupListeners(){
        btn_climate_replay.setOnClickListener {
            if (img_replay.visibility == View.VISIBLE){
                parent.showLoading()
                getUserLocation()
            }
        }

        field4_manual.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                field4_title.text = "UMIDADE ($progress%)"
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                field_climatic_humidity = field4_manual.progress.toDouble()
            }
        })

        field5_manual.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                field5_title.text = "TEMPERATURA (${progress}ºC)"
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
               field_climatic_temperature = field5_manual.progress.toDouble()
            }
        })
    }
    fun setupAds(){
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }
    fun setupPage() {
        parent.showLoading(loading_state_climatic)

        var timeNow = DateTime.now(DateTimeZone.getDefault())
        field1.text = timeNow.toString("dd/MM/yyyy")
        field_dt_start = timeNow

        getUserLocation()
    }

    fun getUserLocation(){
        try {
            var locationUtil = EasyLocationUtility(parent)

            if (locationUtil.permissionIsGranted()) {
                locationUtil.fragmentCheckDeviceSettings(
                    this,
                    EasyLocationUtility.RequestCodes.LAST_KNOWN_LOCATION
                )

                locationUtil.getCurrentLocationOneTime(object : LocationRequestCallback {
                    override fun onLocationResult(location: Location?) {
                        if (location != null) {
                            if (!Session.haveInternet(parent)) {
                                parent.hideLoading()
                                DialogUtils.okDialog(
                                    parent,
                                    "Sem internet!",
                                    "Oops, para carregar os dados climáticos é necessário ter internet.",
                                    {showClimaticManual()}
                                )
                                return
                            }

                            parent.hideLoading()
                            parent.showLoading(loading_state_climatic)
                            loadMap(location.latitude, location.longitude)
                            getClimateInfo(location.latitude, location.longitude)
                        } else {
                            parent.hideLoading()
                        }
                    }

                    override fun onFailedRequest(result: String?) {
                        parent.hideLoading()
                        showClimaticManual()
                    }
                })
            } else {
                locationUtil.fragmentRequestPermission(
                    this,
                    EasyLocationUtility.RequestCodes.LAST_KNOWN_LOCATION
                )
            }
        }catch (ex: Exception){
            parent.log("Quebrou o app, pqp!")
        }
    }
    fun getClimateInfo(latitude: Double, longitude: Double){
        weatherRepo.getWheather(latitude, longitude).observeOnce(parent, Observer {weather->
            parent.hideLoading()
            weather?.results?.let{result->
                field_climatic_description = result.description ?: ""
                field3.text = field_climatic_description

                field_climatic_humidity = result.humidity
                field4.text = field_climatic_humidity.toString()

                field_climatic_temperature = result.temp ?: 0.0
                field5.text = field_climatic_temperature.toString()

                hideClimaticManual()
            }
        })
    }
    fun loadMap(lat: Double, long: Double){
        try {
            val latLong = LatLng(lat, long)
            val mapFragment = SupportMapFragment.newInstance()
            mapFragment.getMapAsync {
                it.mapType = GoogleMap.MAP_TYPE_NORMAL
                val markerOpt = MarkerOptions()
                    .position(latLong)
                    .title("Localização")
                    .snippet("Você está aqui")
                    .visible(true)
                val marker = it.addMarker(markerOpt)
                marker.setAnchor(0f, 0.0f)
                marker.showInfoWindow()
                val camera = CameraPosition.Builder()
                    .target(latLong)
                    .zoom(17.5f)
                    .build()

                val cameraPosition = CameraUpdateFactory.newCameraPosition(camera)
                it.moveCamera(cameraPosition)
                it.uiSettings.setAllGesturesEnabled(false)
                it.uiSettings.isMapToolbarEnabled = false
            }
            childFragmentManager?.beginTransaction()?.replace(R.id.field6, mapFragment)?.commit()
            field6.visibility = View.VISIBLE
        }catch (ex: Exception){
            field6.visibility = View.GONE
            parent.log("Não foi possível carregar o mapa.")
        }
    }
    private fun showClimaticManual(){
        field3.visibility = View.GONE
        field4.visibility = View.GONE
        field5.visibility = View.GONE

        field3_manual.visibility = View.VISIBLE
        field4_manual.visibility = View.VISIBLE
        field5_manual.visibility = View.VISIBLE
        img_replay.visibility = View.VISIBLE

        field4_title.text = "UMIDADE (0%)"
        field5_title.text = "TEMPERATURA (0ºC)"
    }
    private fun hideClimaticManual(){
        field3_manual.visibility = View.GONE
        field4_manual.visibility = View.GONE
        field5_manual.visibility = View.GONE

        field3.visibility = View.VISIBLE
        field4.visibility = View.VISIBLE
        field5.visibility = View.VISIBLE
        img_replay.visibility = View.GONE

        field4_title.text = "UMIDADE (%)"
        field5_title.text = "TEMPERATURA (ºC)"
    }

    private fun clearErrors(){
        field3_error.visibility = View.GONE
    }
    override fun validateFields(): Boolean{
        clearErrors()

        field_description = field2.text.toString()

        if (field3_manual.visibility == View.VISIBLE){
            if (field3_manual.text.isNullOrEmpty()) {
                field3_error.text = "A condição climática é obrigatória"
                field3_error.visibility = View.VISIBLE
            }

            if (field3_error.visibility == View.VISIBLE)
                return false

            field_climatic_description = field3_manual.text.toString()
        }

        return true
    }
    override fun saveFields() {
        try {
            parent.reportFusion.dt_start = field_dt_start
            parent.reportFusion.description = field_description
            parent.reportFusion.climatic_description = field_climatic_description
            parent.reportFusion.climatic_humidity = field_climatic_humidity
            parent.reportFusion.climatic_temperature = field_climatic_temperature
            parent.reportFusion.user_latitude = field_user_latitude
            parent.reportFusion.user_longitude = field_user_longitude
            parent.reportFusion.user_id = Session().user.id
            parent.reportFusion.user_name = Session().user.name
            parent.reportFusion.code = Session().generateReportCode()
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() SUCCESS!")
        }catch (ex: Exception){
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() ERROR: ${ex.message}")
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        var requestGranted = grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED

        if (requestGranted) {
            when (requestCode) {
                EasyLocationUtility.RequestCodes.LAST_KNOWN_LOCATION->{
                    getUserLocation()
                }
            }
        } else {
            parent.hideLoading()
            parent.log("Usuário negou as permissões de localização.")
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK){
            when (requestCode){
                EasyLocationUtility.RequestCodes.LAST_KNOWN_LOCATION->{
                    getUserLocation()
                }
            }
        }else{
            parent.hideLoading()
        }
    }
}

