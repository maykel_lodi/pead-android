package com.lodistudio.pead.ui.reports.reportfusion

import android.Manifest
import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.gms.ads.AdRequest
import com.lodistudio.pead.R
import com.lodistudio.pead.data.repositories.EquipmentRepository
import com.lodistudio.pead.ui.newreport.NewReport
import com.lodistudio.pead.util.AppConstants
import kotlinx.android.synthetic.main.fragment_fusion_step2.*
import org.joda.time.DateTime
import java.util.*
import kotlin.collections.ArrayList


class FusionStep2 : BaseStep() {
    val REQUEST_ID_MULTIPLE_PERMISSIONS = 10
    val CAMERA = 12
    val RESULT_CANCELED = 14

    var field_equipment_manufacturer = ""
    var field_equipment_model = ""
    var field_serie_number = ""
    var field_serie_evidence_uri: Uri? = null
    var field_dt_checked : DateTime? = null
    var field_dt_checked_evidence_uri: Uri? = null

    var evidence_click = 0

    lateinit var equipmentRepo: EquipmentRepository

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fusion_step2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent = context as NewReport

        setupAds()
        setupPage()
        setupListeners()
    }

    fun setupAds(){
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }
    fun setupPage(){
        equipmentRepo = EquipmentRepository.getInstance(parent)!!
        populateManufacturers()
    }
    fun setupListeners(){
        field3_evidence.setOnClickListener {
            evidence_click = 3
            if(checkPermissions()) choosePhotoFromGallery()
        }
        field4.setOnClickListener {
            callDatePicker()
        }
        field4_evidence.setOnClickListener {
            evidence_click = 4
            if(checkPermissions()) choosePhotoFromGallery()
        }
    }


    fun callDatePicker(){
        val datePickerListener = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            if (dayOfMonth == null) return@OnDateSetListener
            if (month == null) return@OnDateSetListener
            if (year == null) return@OnDateSetListener

            val dataEscolhida = "$dayOfMonth/$month/$year"
                field4.text = dataEscolhida
                field_dt_checked = DateTime.parse("$year-$month-$dayOfMonth")
        }

        var dateNow = DateTime.now()
        val datepicker = DatePickerDialog(parent, datePickerListener, dateNow.year,dateNow.monthOfYear,dateNow.dayOfMonth)
        datepicker.datePicker.maxDate = Calendar.getInstance().timeInMillis
        datepicker.show()
    }

    //this method will create and return the path to the image file
    private fun populateManufacturers(){
        field1_loading.visibility = View.VISIBLE
        val manufacturersAdapter = ArrayAdapter<AppConstants.EquipmentManufacturers>(parent,android.R.layout.simple_list_item_1, AppConstants.EquipmentManufacturers.values())
        field1.adapter = manufacturersAdapter
        field1_loading.visibility = View.GONE

        field1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {return}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position > 0) {
                    var manufacturerID = (field1.adapter.getItem(position) as AppConstants.EquipmentManufacturers).id
                    populateModels(manufacturerID)
                }
            }
        }
    }
    private fun populateModels(manufacturerID: String){
        field2_loading.visibility = View.VISIBLE
        val modelsAdapter =
            ArrayAdapter<AppConstants.EquipmentModels>(
                parent,
                android.R.layout.simple_list_item_1,
                AppConstants.EquipmentModels.getModelsByManufacturerID(manufacturerID))
        field2.adapter = modelsAdapter
        field2_loading.visibility = View.GONE
    }
    private fun checkPermissions(): Boolean {
        val camerapermission = ContextCompat.checkSelfPermission(parent, Manifest.permission.CAMERA)
        val writepermission = ContextCompat.checkSelfPermission(parent, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val readpermission = ContextCompat.checkSelfPermission(parent, Manifest.permission.READ_EXTERNAL_STORAGE)

        val listPermissionsNeeded = ArrayList<String>()

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (readpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }

        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
            return false
        }
        return true
    }
    private fun choosePhotoFromGallery() {
        var cameraIntent =  Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        when(evidence_click){
            3->{

                field_serie_evidence_uri = FileProvider.getUriForFile(parent, parent.packageName + ".fileprovider", getFile()!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, field_serie_evidence_uri)
            }
            4->{
                field_dt_checked_evidence_uri = FileProvider.getUriForFile(parent, parent.packageName + ".fileprovider", getFile()!!)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, field_dt_checked_evidence_uri)
            }
        }

        if(cameraIntent.resolveActivity(parent.packageManager)!=null)
        {
            this.startActivityForResult(cameraIntent, CAMERA)
        }
    }
    private fun confirmEvidence(){
        when(evidence_click){
            3->{
                evidence_click = 0
                //field_serie_evidence_uri = uri
                field3_evidence.setImageResource(R.drawable.ic_evidence_ok)
                field3_error.visibility = View.GONE
                field3_evidence.invalidate()
            }
            4->{
                evidence_click = 0
                //field_dt_checked_evidence_uri = uri
                field4_evidence.setImageResource(R.drawable.ic_evidence_ok)
                field4_error.visibility = View.GONE
                field4_evidence.invalidate()
            }
        }
    }
    private fun clearErrors(){
        field1_error.visibility = View.GONE
        field2_error.visibility = View.GONE
        field3_error.visibility = View.GONE
        field4_error.visibility = View.GONE
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {

                val perms = HashMap<String, Int>()
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.READ_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED

                if (grantResults.size > 0) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    if (perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.READ_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    ) {
                        choosePhotoFromGallery()
                    }
                }
            }
        }

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            return
        }
        if (requestCode == CAMERA && resultCode == RESULT_OK) {
            confirmEvidence()
        }
    }
    override fun validateFields(): Boolean{
        clearErrors()

        if(field1.adapter == null || field1.selectedItem == null){
            field1_error.text = "O fabricante do equipamento é obrigatório"
            field1_error.visibility = View.VISIBLE
        }else {
            var field1Selected = field1.selectedItem as AppConstants.EquipmentManufacturers
            if (field1Selected.id == "") {
                field1_error.text = "O fabricante do equipamento é obrigatório"
                field1_error.visibility = View.VISIBLE
            }
        }

        if(field2.adapter == null || field2.selectedItem == null){
            field2_error.text = "O fabricante do equipamento é obrigatório"
            field2_error.visibility = View.VISIBLE
        }else{
            var field2Selected = field2.selectedItem as AppConstants.EquipmentModels
            if (field2Selected.publicName == ""){
                field2_error.text = "O fabricante do equipamento é obrigatório"
                field2_error.visibility = View.VISIBLE
            }
        }

        if (field3.text.isNullOrEmpty()){
            field3_error.text = "O número de série do equipamento é obrigatório"
            field3_error.visibility = View.VISIBLE
        }else if(field_serie_evidence_uri == null){
            field3_error.text = "Você precisa evidenciar o número de série antes de continuar"
            field3_error.visibility = View.VISIBLE
        }

        if (field_dt_checked == null) {
            field4_error.text = "A data de aferição do equipamento é obrigatória"
            field4_error.visibility = View.VISIBLE
        }else if(field_dt_checked_evidence_uri == null){
            field4_error.text = "Você precisa evidenciar a data de aferição antes de continuar"
            field4_error.visibility = View.VISIBLE
        }

        if (field1_error.visibility == View.VISIBLE ||
            field2_error.visibility == View.VISIBLE ||
            field3_error.visibility == View.VISIBLE ||
            field4_error.visibility == View.VISIBLE)
            return false

        field_equipment_manufacturer = (field1.selectedItem as AppConstants.EquipmentManufacturers).publicName
        field_equipment_model = (field2.selectedItem as AppConstants.EquipmentModels).publicName
        field_serie_number = field3.text.toString()

        return true
    }
    override fun saveFields() {
        try {
            parent.reportFusion.equipment_manufacturer_name = field_equipment_manufacturer
            parent.reportFusion.equipment_model_name = field_equipment_model
            parent.reportFusion.equipment_serie_number = field_serie_number
            parent.reportFusion.equipment_serie_number_evidence = field_serie_evidence_uri.toString()
            parent.reportFusion.equipment_dt_checked = field_dt_checked
            parent.reportFusion.equipment_dt_checked_evidence = field_dt_checked_evidence_uri.toString()
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() SUCCESS!")
        }catch (ex: Exception){
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() ERROR: ${ex.message}")
        }
    }
}
