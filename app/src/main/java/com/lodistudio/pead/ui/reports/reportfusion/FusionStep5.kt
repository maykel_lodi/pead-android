package com.lodistudio.pead.ui.reports.reportfusion

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.gms.ads.AdRequest

import com.lodistudio.pead.R
import com.lodistudio.pead.ui.newreport.NewReport
import com.lodistudio.pead.util.AppConstants
import kotlinx.android.synthetic.main.fragment_fusion_step5.*
import java.lang.Exception
import java.math.BigDecimal
import java.util.HashMap

class FusionStep5 : BaseStep() {
    val REQUEST_ID_MULTIPLE_PERMISSIONS = 10
    val CAMERA = 12
    val RESULT_CANCELED = 14

    var field_weld_pressure = 0.0
    var field_weld_drag_pressure = 0
    var field_weld_pipe_wall = 0.0
    var field_weld_initial_cord_height = 0.0
    var field_weld_heating_plate_temperature = 0.0
    var field_weld_heating_plate_temperature_evidence: Uri? = null
    var field_weld_heating_total_time = 0.0
    var field_weld_cooling_total_time = 0.0
    var field_weld_plate_out_time = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fusion_step5, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent = context as NewReport

        setupAds()
        setupPage()
    }

    fun setupAds(){
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    fun setupPage(){
        setupListeners()
        populateWeldRules()
    }

    private fun setupListeners(){
        field3.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                field3_value.text = progress.toString()
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                calculatePressure()
            }
        })

        field5_evidence.setOnClickListener {
            if(checkPermissions()) choosePhotoFromGallery()
        }
    }

    private fun calculatePressure(){
        //Recursos do calculo
        var eManufacturer = parent.reportFusion.equipment_manufacturer_name
        var eModel = parent.reportFusion.equipment_model_name
        var equipamentPistonArea = AppConstants.EquipmentManufacturers.getModelPistonArea(eManufacturer!!, eModel!!)
        var weldRule = (field1.selectedItem as AppConstants.WeldRules)
        var externalDiameter = parent.reportFusion.material_1_external_diameter
        var sdr = parent.reportFusion.material_1_sdr
        var pi = 3.1416
        var dragPressure = field3.progress


        //Parede do tubo
        val pipeWall = externalDiameter!!.div(sdr!!)
        field_weld_pipe_wall = pipeWall

        //Calculo da altura do cordão inicial
        val heightInitialCord = BigDecimal(0.15 * pipeWall).setScale(2, BigDecimal.ROUND_DOWN).toDouble()
        field4.text = "$heightInitialCord mm"
        field_weld_initial_cord_height = heightInitialCord

        //Calculo da pressão
        var pressure = ((externalDiameter - pipeWall) * pipeWall * pi * weldRule.pressure_factor) / equipamentPistonArea
        var finalPressure = pressure + dragPressure

        //Calculo do Tempo de Aquecimento
        field_weld_heating_total_time = (pipeWall*weldRule.heating_time_factor)
        //Calculo do Tempo de Retirada da placa
        field_weld_plate_out_time = ((pipeWall*0.1) + 8)
        //Calculo do Tempo de Resfriamento
        field_weld_cooling_total_time = (pipeWall*weldRule.freezing_time_factor)

        var fixedPressure =  BigDecimal(finalPressure).setScale(2, BigDecimal.ROUND_DOWN).toDouble()
        field_weld_pressure = fixedPressure

        field2.text = "${fixedPressure} bar"
    }
    private fun populateWeldRules(){
        var wRulesAdapter = ArrayAdapter<AppConstants.WeldRules>(parent,android.R.layout.simple_list_item_1, AppConstants.WeldRules.values())
        field1.adapter = wRulesAdapter

        field1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {return}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                calculatePressure()
            }
        }
    }

    private fun checkPermissions(): Boolean {
        val camerapermission = ContextCompat.checkSelfPermission(parent, Manifest.permission.CAMERA)
        val writepermission = ContextCompat.checkSelfPermission(parent, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val readpermission = ContextCompat.checkSelfPermission(parent, Manifest.permission.READ_EXTERNAL_STORAGE)

        val listPermissionsNeeded = ArrayList<String>()

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (readpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }

        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
            return false
        }
        return true
    }
    private fun choosePhotoFromGallery() {
        var cameraIntent =  Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        field_weld_heating_plate_temperature_evidence = FileProvider.getUriForFile(parent, parent.packageName + ".fileprovider", getFile()!!)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, field_weld_heating_plate_temperature_evidence)

        if(cameraIntent.resolveActivity(parent.packageManager)!=null)
        {
            this.startActivityForResult(cameraIntent, CAMERA)
        }
    }
    private fun confirmEvidence(){
        field5_evidence.setImageResource(R.drawable.ic_evidence_ok)
        field5_error.visibility = View.GONE
        field5_evidence.invalidate()
    }

    private fun clearErrors(){
        field5_error.visibility = View.GONE
    }

    override fun validateFields(): Boolean{
        clearErrors()

        if(field5.text.isNullOrEmpty()){
            field5_error.text = "A temperatura da placa de aquecimento é obrigatória"
            field5_error.visibility = View.VISIBLE
        }else if(field_weld_heating_plate_temperature_evidence == null){
            field5_error.text = "Você precisa evidenciar a temperatura da placa antes de continuar"
            field5_error.visibility = View.VISIBLE
        }

        if (field5_error.visibility == View.VISIBLE)
            return false

        field_weld_drag_pressure = field3.progress
        field_weld_heating_plate_temperature = field5.text.toString().toDoubleOrNull() ?: 0.0

        return true
    }
    override fun saveFields() {
        try {
            parent.reportFusion.weld_pressure = field_weld_pressure
            parent.reportFusion.weld_drag_pressure = field_weld_drag_pressure
            parent.reportFusion.weld_pipe_wall = field_weld_pipe_wall
            parent.reportFusion.weld_initial_cord_height = field_weld_initial_cord_height
            parent.reportFusion.weld_heating_plate_temperature = field_weld_heating_plate_temperature
            parent.reportFusion.weld_heating_plate_temperature_evidence = field_weld_heating_plate_temperature_evidence.toString()
            parent.reportFusion.weld_heating_total_time = field_weld_heating_total_time
            parent.reportFusion.weld_cooling_total_time = field_weld_cooling_total_time
            parent.reportFusion.weld_plate_out_time = field_weld_plate_out_time
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() SUCCESS!")
        }catch (ex: Exception){
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() ERROR: ${ex.message}")
        }
    }
    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            calculatePressure()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {

                val perms = HashMap<String, Int>()
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.READ_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED

                if (grantResults.isNotEmpty()) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    if (perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.READ_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    ) {
                        choosePhotoFromGallery()
                    }
                }
            }
        }

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            return
        }
        if (requestCode == CAMERA && resultCode == RESULT_OK) {
            confirmEvidence()
        }
    }

}
