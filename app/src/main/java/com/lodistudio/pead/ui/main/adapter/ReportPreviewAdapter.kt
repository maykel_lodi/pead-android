package com.lodistudio.pead.ui.main.adapter

import android.content.Context
import android.text.format.DateUtils
import android.view.*
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lodistudio.pead.R
import com.lodistudio.pead.data.local.entity.custom.ReportPreviewInfo
import com.lodistudio.pead.util.DialogUtils
import kotlinx.android.synthetic.main.report_preview.view.*
import org.joda.time.Seconds


class ReportPreviewAdapter(
    val contextParent: Context,
    var reportList: List<ReportPreviewInfo>,
    val generatePdfClick: (String)->Unit):
    RecyclerView.Adapter<ReportPreviewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.report_preview, viewGroup, false)

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return reportList.size
    }

    override fun onBindViewHolder(view: ViewHolder, position: Int) {
        view.bindItems(reportList[position], contextParent)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(report: ReportPreviewInfo, context: Context) {

            itemView.report_number.text = report.code
            itemView.description.text = if (report.description.isNullOrEmpty()) "DESCRIÇÃO NÃO INFORMADA" else report.description

            if(report.dt_aborted == null && report.dt_finished != null){
                var finishedIcon = ContextCompat.getDrawable(context, R.drawable.ic_report_status_finished)
                itemView.report_status_icon.setImageDrawable(finishedIcon)
                itemView.report_status.text = "FINALIZADO"
                itemView.report_status.setTextColor(ContextCompat.getColor(context, R.color.report_status_finished))

                var totalTime = Seconds.secondsBetween(report.dt_start?.toInstant(), report.dt_finished?.toInstant())
                itemView.report_time.text = "${DateUtils.formatElapsedTime(totalTime.seconds.toLong())}"

            }else if(report.dt_aborted != null){
                var abortedIcon = ContextCompat.getDrawable(context, R.drawable.ic_report_status_aborted)
                itemView.report_status_icon.setImageDrawable(abortedIcon)
                itemView.report_status.text = "ABORTADO"
                itemView.report_status.setTextColor(ContextCompat.getColor(context, R.color.report_status_aborted))
                itemView.report_time.text = "00:00"
            }

            itemView.report_options.setOnClickListener{ v ->
                val menuBuilder = MenuBuilder(contextParent)
                val inflater = MenuInflater(contextParent)
                inflater.inflate(R.menu.report_menu, menuBuilder)
                val optionsMenu = MenuPopupHelper(contextParent, menuBuilder, v)
                optionsMenu.setForceShowIcon(true)

                if(report.dt_aborted != null){
                    menuBuilder.findItem(R.id.menu_export_report).isVisible = false
                    menuBuilder.findItem(R.id.menu_show_message).isVisible = true
                }else{
                    menuBuilder.findItem(R.id.menu_export_report).isVisible = true
                    menuBuilder.findItem(R.id.menu_show_message).isVisible = false
                }


                menuBuilder.setCallback(object : MenuBuilder.Callback {
                    override fun onMenuItemSelected(menu: MenuBuilder, item: MenuItem): Boolean {
                        when (item.itemId) {
                            R.id.menu_export_report ->{
                                generatePdfClick(report.id ?: "")
                            }
                            R.id.menu_show_message->{
                                DialogUtils.okDialog(
                                    context,
                                    "Motivo do cancelamento",
                                    report.aborted_report_reason.toString(),
                                    {}
                                )
                            }
                            else -> return false
                        }
                        return true
                    }

                    override fun onMenuModeChange(menu: MenuBuilder) {}
                })
                optionsMenu.show()
            }

        }
    }
}