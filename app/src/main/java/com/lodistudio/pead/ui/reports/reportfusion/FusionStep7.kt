package com.lodistudio.pead.ui.reports.reportfusion

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.ads.AdRequest

import com.lodistudio.pead.R
import com.lodistudio.pead.ui.newreport.NewReport
import com.lodistudio.pead.util.AppConstants
import kotlinx.android.synthetic.main.fragment_fusion_step7.*
import org.joda.time.DateTime

class FusionStep7 : BaseStep() {
    var field_weld_approved = true
    var field_equipment_condition = ""
    var field_weld_final_width_strings_side1 = 0.0
    var field_weld_final_width_strings_side2 = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fusion_step7, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent = context as NewReport

        setupAds()
        setupPage()
    }

    fun setupAds(){
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    fun setupPage(){
        field4_approved.isChecked = true
    }

    fun clearErrors(){
        field1_error.visibility = View.GONE
        field2_error.visibility = View.GONE
    }

    override fun validateFields(): Boolean{
        clearErrors()

        if(field1.text.isNullOrEmpty()){
            field1_error.text = "A Largura final dos cordões é obrigatória"
            field1_error.visibility = View.VISIBLE
        }

        if(field2.text.isNullOrEmpty()){
            field2_error.text = "A Largura final dos cordões é obrigatória"
            field2_error.visibility = View.VISIBLE
        }

        if( field1_error.visibility == View.VISIBLE ||
            field2_error.visibility == View.VISIBLE)
            return false

        field_weld_approved = field4_approved.isChecked
        field_equipment_condition = field3.text.toString()
        field_weld_final_width_strings_side1 = field1.text.toString().toDouble()
        field_weld_final_width_strings_side2 = field2.text.toString().toDouble()

        return true
    }
    override fun saveFields() {
        try {
            parent.reportFusion.report_weld_approved = field_weld_approved
            parent.reportFusion.equipment_condition = field_equipment_condition
            parent.reportFusion.weld_final_width_strings_side1 = field_weld_final_width_strings_side1
            parent.reportFusion.weld_final_width_strings_side2 = field_weld_final_width_strings_side2
            parent.reportFusion.dt_finished = DateTime.now()
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() SUCCESS!")
        }catch (ex: Exception){
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() ERROR: ${ex.message}")
        }
    }
}
