package com.lodistudio.pead.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.lodistudio.pead.R
import com.lodistudio.pead.data.repositories.UserRepository
import com.lodistudio.pead.ui.main.MainActivity
import com.lodistudio.pead.ui.register.RegisterActivity
import com.lodistudio.pead.util.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {
    lateinit var userRepo: UserRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        userRepo = UserRepository.getInstance(this)!!

        checkSession()
        setupListeners()
    }

    private fun setupListeners(){
        btn_register.setOnClickListener {
            var intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        btn_login.setOnClickListener {
            validateLogin()
        }
    }

    fun checkSession(){
        if(Session().user != null && !Session().user.id.isNullOrEmpty()){
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finishAfterTransition()
        }
    }

    fun validateLogin(){
        input_email.error = null
        input_password.error = null

        if(input_email.text.isNullOrEmpty()){
            input_email.error = ValidationErrors.requiredFieldError
        }else if(!ValidationUtils.isValidEmail(input_email.text.toString())){
            input_email.error = ValidationErrors.emailInvalidError
        }

        if(input_password.text.isNullOrEmpty()){
            input_password.error = ValidationErrors.requiredFieldError
        }else if(!ValidationUtils.isValidPassword(input_password.text.toString())){
            input_password.error = ValidationErrors.passwordInvalidLengthError
        }

        if(!input_email.error.isNullOrEmpty() || !input_password.error.isNullOrEmpty())
            return
        showLoading()
        userRepo.getUserByEmail(input_email.text.toString()).observeOnce(this, Observer {user->
            if(user != null && user.password == input_password.text.toString()){
                Session().user = user
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finishAfterTransition()
            }else{
                hideLoading()
                toast("E-mail/Senha incorretos")
            }
        })
    }

}
