package com.lodistudio.pead.ui.reports.reportfusion.template

import android.text.format.DateUtils
import com.lodistudio.pead.data.local.entity.ReportFusion

class ReportFusionPdfTemplate {
    companion object{
        fun createHtmlTemplate(report: ReportFusion): String{
            var html = ""

            html += "<style>"
            html += "        table{"
            html += "            border-collapse: collapse;"
            html += "            width: 100%;"
            html += "            font-family: Verdana, Arial, Helvetica, sans-serif;"
            html += "        }"
            html += "table, th, td {"
            html += "    border: 1px solid #ddd;"
            html += "    text-align: center;"
            html += "    padding: 8px;"
            html += "}"
            html += "</style>"


            html += "<table>"
            html += "<tr>"
            html += "<th colspan='2'><b>RELATÓRIO DE SOLDA TERMOFUSÃO</b></th>"
            html += "</tr>"
            html += "<tr>"
            html += "<td><b>Relatório:</b> ${report.code}</td>"
            html += "<td><b>Soldador:</b> ${report.user_name}</td>"
            html += "</tr>"

            html += "<tr>"
            html += "<td><b>Data execução:</b> ${report.dt_start?.toString("dd/MM/yyyy HH:MM:ss") ?: ""}</td>"
            html += "<td><b>Tipo de solda:</b> ${report.material_1_type}/${report.material_2_type}</td>"
            html += "</tr>"

            html += "<tr>"
            html += "<td colspan='2'><b>Condições Climáticas:</b> " +
                    "${report.climatic_description} / " +
                    "Umidade ${report.climatic_humidity}% / " +
                    "Temperatura ${report.climatic_temperature}ºC</td>"
            html += "</tr>"

            html += "<tr>"
            html += "<td colspan='2'><b>Descrição do trecho:</b> ${report.description}</td>"
            html += "</tr>"
            html += "</table>"

            html += "<br>"

            html += "<table>"

            html += "<tr>"
            html += "<th colspan='7'>${report.material_1_type}</th>"
            html += "</tr>"
            html += "<tr>"
            html += "<th>Fabricante</th>"
            html += "<th>Diametro Externo (DE)</th>"
            html += "<th>Tipo Resina</th>"
            html += "<th>PN</th>"
            html += "<th>SDR</th>"
            html += "<th>Nº Lote</th>"
            html += "<th>Norma</th>"
            html += "</tr>"
            html += "<tr>"
            html += "<td>${report.material_1_manufacturer}</td>"
            html += "<td>${report.material_1_external_diameter} mm</td>"
            html += "<td>${report.material_1_resin_type}</td>"
            html += "<td>${report.material_1_pn}</td>"
            html += "<td>${report.material_1_sdr}</td>"
            html += "<td>${report.material_1_lote_number}</td>"
            html += "<td>${report.material_1_rule}</td>"
            html += "</tr>"
            html +="<tr>"
            html +="<th colspan='7'>${report.material_2_type}</th>"
            html +="</tr>"
            html +="<tr>"
            html +="<th>Fabricante</th>"
            html +="<th>Diametro Externo (DE)</th>"
            html +="<th>Tipo Resina</th>"
            html +="<th>PN</th>"
            html +="<th>SDR</th>"
            html +="<th>Nº Lote</th>"
            html +="<th>Norma</th>"
            html +="</tr>"
            html +="<tr>"
            html += "<td>${report.material_2_manufacturer}</td>"
            html += "<td>${report.material_2_external_diameter} mm</td>"
            html += "<td>${report.material_2_resin_type}</td>"
            html += "<td>${report.material_2_pn}</td>"
            html += "<td>${report.material_2_sdr}</td>"
            html += "<td>${report.material_2_lote_number}</td>"
            html += "<td>${report.material_2_rule}</td>"
            html +="</tr>"
            html +="</table>"

             html +="<br>"

             html +="<table>"

             html +="<tr>"
             html +="<th colspan='2'>PARAMETROS DE SOLDAGEM</th>"
             html +="</tr>"

             html +="<tr>"
             html +="<td>Hora de início da soldagem</td>"
             html +="<td>${report.weld_dt_process_start?.toString("dd/MM/yyyy HH:MM:ss") ?: ""}</td>"
             html +="</tr>"
             html +="<tr>"
             html +="<td>Pressão de arraste</td>"
             html +="<td>${report.weld_drag_pressure} bar</td>"
             html +="</tr>"
             html +="<tr>"
             html +="<td>Pressão de soldagem</td>"
             html +="<td>${report.weld_pressure} bar</td>"
             html +="</tr>"
             html +="<tr>"
             html +="<td>Tempreratura da placa de aquecimento</td>"
             html +="<td>${report.weld_heating_plate_temperature}ºC</td>"
             html +="</tr>"
             html +="<tr>"
             html +="<td>Altura do cordão inicial</td>"
             html +="<td>${report.weld_initial_cord_height} mm</td>"
             html +="</tr>"
             html +="<tr>"
             html +="<td>Tempo de aquecimento</td>"
             html +="<td>${DateUtils.formatElapsedTime(report.weld_heating_total_time!!.toLong())}</td>"
             html +="</tr>"
             html +="<tr>"
             html +="<td>Tempo de resfriamento</td>"
             html +="<td>${DateUtils.formatElapsedTime(report.weld_cooling_total_time!!.toLong())}</td>"
             html +="</tr>"
             html +="<tr>"
             html +="<td>Largura final dos cordões (L1 + L2)</td>"
             html +="<td>${(report.weld_final_width_strings_side1 ?: 0.0) + (report.weld_final_width_strings_side2 ?: 0.0)} mm</td>"
             html +="</tr>"

            html +="</table>"

            html +="<br>"

            html +="<table>"


            html +="<tr>"
            html +="<th colspan='2'>Avaliação</th>"
            html +="</tr>"

            html +="<tr>"
            html +="<td>Conclusão final</td>"
            html +="<td>[${if (report.report_weld_approved == true) "X" else " "}] Solda Aprovada  [${if (report.report_weld_approved == false) "X" else " "}] Solda Reprovada</td>"
            html +="</tr>"

            html +="</table>"


            return html
        }
    }
}