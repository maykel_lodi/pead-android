package com.lodistudio.pead.ui.reports.reportfusion

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdRequest
import com.lodistudio.pead.R
import com.lodistudio.pead.ui.newreport.NewReport
import com.lodistudio.pead.ui.reports.reportfusion.adapter.WeldTypeAdapter
import com.lodistudio.pead.util.AppConstants
import kotlinx.android.synthetic.main.fragment_fusion_step3.*

class FusionStep3 : BaseStep() {
    var field_weld_type_1 = ""
    var field_weld_type_2 = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fusion_step3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent = context as NewReport

        setupAds()
        setupPage()
    }

    fun setupAds(){
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }
    fun setupPage(){
        val layoutManager1 = LinearLayoutManager(parent, LinearLayoutManager.VERTICAL, false)
        val conexoes1Adapter = WeldTypeAdapter(parent, null, selectType1)
        conexoes1.layoutManager = layoutManager1
        conexoes1.adapter = conexoes1Adapter

        val layoutManager2 = LinearLayoutManager(parent, LinearLayoutManager.VERTICAL, false)
        val conexoes2Adapter = WeldTypeAdapter(parent, null, selectType2)
        conexoes2.layoutManager = layoutManager2
        conexoes2.adapter = conexoes2Adapter

    }

    val selectType1: (AppConstants.WeldTypes?) -> Unit = {
        field_weld_type_1 = it?.publicName ?: ""
    }
    val selectType2: (AppConstants.WeldTypes?) -> Unit = {
        field_weld_type_2 = it?.publicName ?: ""
    }

    private fun clearErrors(){
        field_error.visibility = View.GONE
    }

    override fun validateFields(): Boolean{
        clearErrors()

        if (field_weld_type_1.isNullOrEmpty() || field_weld_type_2.isNullOrEmpty()){
            field_error.text = "Os tipos das conexões 1 e 2 são obrigatórios"
            field_error.visibility = View.VISIBLE
        }

        if(field_error.visibility == View.VISIBLE) return false

        return true
    }
    override fun saveFields() {
        try {
            parent.reportFusion.material_1_type = field_weld_type_1
            parent.reportFusion.material_2_type = field_weld_type_2
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() SUCCESS!")
        }catch (ex: Exception){
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() ERROR: ${ex.message}")
        }
    }
}
