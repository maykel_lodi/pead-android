package com.lodistudio.pead.ui.reports.reportfusion

import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.ads.AdRequest
import com.lodistudio.pead.BuildConfig
import com.lodistudio.pead.R
import com.lodistudio.pead.ui.newreport.NewReport
import com.lodistudio.pead.util.AppConstants
import kotlinx.android.synthetic.main.fragment_fusion_step6.*
import kotlinx.android.synthetic.main.fragment_fusion_step6.adView
import kotlinx.android.synthetic.main.fragment_fusion_step6.field1
import kotlinx.android.synthetic.main.fragment_fusion_step6.field2
import kotlinx.android.synthetic.main.fragment_fusion_step6.field3
import kotlinx.android.synthetic.main.fragment_fusion_step6.field4
import kotlinx.android.synthetic.main.fragment_fusion_step6.field5
import org.joda.time.DateTime

class FusionStep6 : BaseStep() {
    var field_weld_dt_process_start: DateTime? = null
    var field_weld_dt_process_end: DateTime? = null
    lateinit var beep: MediaPlayer

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fusion_step6, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent = context as NewReport
        beep = MediaPlayer.create(parent, R.raw.beep)

        parent.blockNext(true)
        setupAds()
        setupListeners()
        setupPage()
    }

    fun setupAds() {
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun setupListeners() {
        btn_start.setOnClickListener {
            field_weld_dt_process_start = DateTime.now()

            btn_start.visibility = View.GONE
            field1.text = "Aguarde o tempo de aquecimento..."
            if(BuildConfig.BUILD_TYPE.equals("debug"))
                heatingTimer(5*1000)
            else
                heatingTimer(parent.reportFusion.weld_heating_total_time!!.toLong()*1000)
            //heatingTimer(5000)
            field2.visibility = View.VISIBLE
        }
    }

    private fun setupPage() {
        var heatTime =
            DateUtils.formatElapsedTime(parent.reportFusion.weld_heating_total_time!!.toLong())
        var freezeTime =
            DateUtils.formatElapsedTime(parent.reportFusion.weld_cooling_total_time!!.toLong())
        var plateTime =
            DateUtils.formatElapsedTime(parent.reportFusion.weld_plate_out_time!!.toLong())

        field3.text = heatTime
        field4.text = plateTime
        field5.text = freezeTime

        field1.text = "Cronômetro de solda"
    }

    fun heatingTimer(milis: Long) {
        object : CountDownTimer(milis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var timeLeft = millisUntilFinished / 1000
                field2.text = "${DateUtils.formatElapsedTime(timeLeft)}"
            }

            override fun onFinish() {
                beep.start()
                field1.text = "Retire a placa..."
                field3_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_ok, 0)
                if(BuildConfig.BUILD_TYPE.equals("debug"))
                    plateOutTimer(5*1000)
                else
                    plateOutTimer(parent.reportFusion.weld_plate_out_time!!.toLong()*1000)
                //plateOutTimer(5000)
            }
        }.start()
    }

    fun plateOutTimer(milis: Long) {
        object : CountDownTimer(milis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var timeLeft = millisUntilFinished / 1000
                field2.text = "${DateUtils.formatElapsedTime(timeLeft)}"
            }
            override fun onFinish() {
                beep.start()
                field4_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_ok, 0)
                field1.text = "Aguarde o tempo de resfriamento..."
                if(BuildConfig.BUILD_TYPE.equals("debug"))
                    freezingTimer(5*1000)
                else
                    freezingTimer(parent.reportFusion.weld_cooling_total_time!!.toLong()*1000)
                //freezingTimer(5000)
            }
        }.start()
    }

    fun freezingTimer(milis: Long){
        object : CountDownTimer(milis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var timeLeft = millisUntilFinished / 1000
                field2.text = "${DateUtils.formatElapsedTime(timeLeft)}"
            }
            override fun onFinish() {
                beep.start()
                field5_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_ok, 0)
                parent.blockNext(false)
                field2.visibility = View.GONE
                btn_start.visibility = View.VISIBLE
                field_weld_dt_process_end = DateTime.now()
            }
        }.start()
    }

    override fun validateFields(): Boolean{
        return true
    }
    override fun saveFields() {
        try{
            parent.reportFusion.weld_dt_process_start = field_weld_dt_process_start
            parent.reportFusion.weld_dt_process_end = field_weld_dt_process_end
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() SUCCESS!")
        }catch (ex: Exception){
            Log.d(AppConstants.logTitle, "${this::class.java.simpleName} - saveFields() ERROR: ${ex.message}")
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            if (btn_start.visibility == View.GONE) parent.blockNext(true)
            else parent.blockNext(false)

        }
    }
}
