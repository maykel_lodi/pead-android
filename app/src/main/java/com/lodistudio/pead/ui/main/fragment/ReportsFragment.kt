package com.lodistudio.pead.ui.main.fragment

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment.getExternalStorageDirectory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.lodistudio.pead.R
import com.lodistudio.pead.data.repositories.ReportFusionRepository
import com.lodistudio.pead.ui.main.MainActivity
import com.lodistudio.pead.ui.main.adapter.ReportPreviewAdapter
import com.lodistudio.pead.ui.newreport.NewReport
import com.lodistudio.pead.ui.reports.reportfusion.template.ReportFusionPdfTemplate
import com.lodistudio.pead.util.DialogUtils
import com.lodistudio.pead.util.Session
import com.lodistudio.pead.util.observeOnce
import io.github.lucasfsc.html2pdf.Html2Pdf
import kotlinx.android.synthetic.main.fragment_reports.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.joda.time.DateTime
import java.io.File


class ReportsFragment : Fragment() {
    lateinit var reportRepo: ReportFusionRepository
    lateinit var parent: MainActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_reports, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent = context as MainActivity
        reportRepo = ReportFusionRepository.getInstance(parent)!!

        setupListeners()
        setupReports()
    }

    fun setupReports(){
        parent.showLoading()
        reportRepo.getAllPreview().observeOnce(parent, Observer {result->
            parent.hideLoading()
            if(result.isNotEmpty()){
                reportList.adapter = null
                var lManager = LinearLayoutManager(parent, LinearLayoutManager.VERTICAL, false)
                var reportAdapter = ReportPreviewAdapter(parent, result.sortedByDescending { r->r.dt_start },generatePDF)
                reportList.layoutManager = lManager
                reportList.adapter = reportAdapter
            }

        })
    }
    fun setupListeners(){
        fab_add.setOnClickListener {
            var intent = Intent(parent, NewReport::class.java)
            startActivity(intent)
            parent.finishAfterTransition()
        }
    }

    private val generatePDF: (String) -> Unit = { reportID ->

        if (!Session.haveInternet(parent)) {
            parent.hideLoading()
            DialogUtils.okDialog(
                parent,
                "Sem internet!",
                "Oops, para exportar o relatório é necessário ter internet.",
                {}
            )
        }else {
            parent.showLoading()
            reportRepo.getReportByID(reportID).observeOnce(parent, Observer { report ->
                if (report != null && !report.id.isNullOrEmpty()) {
                    val fileName = "${reportID}-${DateTime.now()}.pdf"
                    val storagePath =
                        "${getExternalStorageDirectory().absolutePath}/PeadPDF/$fileName"
                    var filePDF = File(storagePath)
                    filePDF.parentFile.mkdirs()

                    val fileUri = Uri.fromFile(filePDF)

                    val permissionCheck =
                        ContextCompat.checkSelfPermission(parent, WRITE_EXTERNAL_STORAGE)
                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                            arrayOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE),
                            100
                        )
                    }

                    val converter = Html2Pdf.Companion.Builder()
                        .context(parent)
                        .html(ReportFusionPdfTemplate.createHtmlTemplate(report))
                        .file(filePDF)
                        .build()

                    converter.convertToPdf(object : Html2Pdf.OnCompleteConversion {
                        override fun onSuccess() {
                            val map = HashMap<String, RequestBody>()
                            map["fileName"] =
                                RequestBody.create(MediaType.parse("multipart/form-data"), fileName)
                            map["emailTarget"] = RequestBody.create(
                                MediaType.parse("multipart/form-data"),
                                Session().user.email
                            )
                            map["subject"] = RequestBody.create(
                                MediaType.parse("multipart/form-data"),
                                "Relatório de Solda"
                            )

                            val requestFile =
                                RequestBody.create(MediaType.parse("multipart/form-data"), filePDF)
                            val filePart =
                                MultipartBody.Part.createFormData("file", fileName, requestFile)


                            reportRepo.sendReportEmail(map, filePart)
                                .observeOnce(this@ReportsFragment, Observer {
                                    parent.hideLoading()
                                    if (it != null && it.isSuccess && it.data) {
                                        parent.toast("PDF gerado e enviado para o e-mail ${Session().user.email} com sucesso.")
                                    } else {
                                        parent.toast("Não foi possível enviar o relatório, tente novamente mais tarde.")
                                    }
                                })

//   val intent = Intent(Intent.ACTION_VIEW)
//   var uriOpen = FileProvider.getUriForFile(parent, parent.packageName + ".fileprovider", filePDF)
//   intent.setDataAndType(uriOpen, "application/pdf")
//   startActivity(intent)
                        }

                        override fun onFailed() {
                            parent.hideLoading()
                            parent.toast("Falha ao gerar pdf.")
                        }
                    })
                }
            })
        }
    }
}
