package com.lodistudio.pead.ui.newreport

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import com.lodistudio.pead.R
import com.lodistudio.pead.data.local.entity.ReportFusion
import com.lodistudio.pead.data.repositories.ReportFusionRepository
import com.lodistudio.pead.ui.main.MainActivity
import com.lodistudio.pead.ui.reports.reportfusion.*
import com.lodistudio.pead.util.BaseActivity
import kotlinx.android.synthetic.main.activity_new_report.*
import kotlinx.android.synthetic.main.activity_new_report.btn_abort
import kotlinx.android.synthetic.main.dialog_abort_report.view.*
import org.joda.time.DateTime

class NewReport : BaseActivity() {
    var stepList = listOf(FusionStep1(), FusionStep2(), FusionStep3(), FusionStep4(), FusionStep5(), FusionStep6(), FusionStep7())
    var reportFusion = ReportFusion()
    var currentStep = 1

    var user_lat = 0.0
    var user_long = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_report)

        setupToolbar()
        setupListeners()

        loadStep(currentStep)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
    }
    private fun setupListeners(){
        btn_abort.setOnClickListener {
            abortReport()
        }
        btn_next.setOnClickListener {
            goNextStep()
        }
    }

    fun goNextStep(){
        if(!validateStep(currentStep)) return
        saveStep(currentStep)

        if(isLastStep(currentStep)) finishReport()
        else currentStep++

        if (isLastStep(currentStep)) btn_next.text = "FINALIZAR"
        else btn_next.text = "AVANÇAR"

        loadStep(currentStep)
    }
    fun goPreviousStep(){
        if(currentStep == 1) {
            goBackToMain()
        }else{
            currentStep--
            if (isLastStep(currentStep)) btn_next.text = "FINALIZAR"
            else btn_next.text = "AVANÇAR"
            loadStep(currentStep)
        }
        blockNext(false)
    }
    fun abortReport(){
        val view: View = LayoutInflater.from(this).inflate(R.layout.dialog_abort_report, null, false)

        val builder = AlertDialog.Builder(this)
            .setView(view)
            .setCancelable(true)
            .create()

        view.title.text = "Abortar Relatório"
        view.btn_abort.setOnClickListener {
            builder.dismiss()
            reportFusion.aborted_report_reason = view.reason.text.toString()
            reportFusion.dt_aborted = DateTime.now()
            saveStep(currentStep)
            finishReport()
        }
        builder.setCanceledOnTouchOutside(true)
        builder.window.setBackgroundDrawable(ContextCompat.getDrawable(this, android.R.color.transparent))
        builder.show()
    }
    fun blockNext(block: Boolean){
        if (block){
            btn_next.background = ContextCompat.getDrawable(this, R.color.btn_disabled_bg)
            btn_next.isClickable = false
            btn_next.isFocusable = false
            btn_next.isFocusableInTouchMode = false
            btn_next.isEnabled = false
        }else{
            btn_next.background = ContextCompat.getDrawable(this, R.drawable.btn_bluesquare_sel)
            btn_next.isClickable = true
            btn_next.isFocusable = true
            btn_next.isFocusableInTouchMode = false
            btn_next.isEnabled = true
        }
    }

    private fun finishReport(){

        var reportRepo = ReportFusionRepository.getInstance(this)
        reportRepo!!.insert(reportFusion)

        Toast.makeText(this, "Relatório salvo com sucesso.", Toast.LENGTH_LONG).show()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finishAfterTransition()
    }
    private fun saveStep(stepNumber: Int){
        stepList[stepNumber-1].saveFields()
    }
    private fun validateStep(stepNumber: Int): Boolean{
        return stepList[stepNumber-1].validateFields()
    }
    private fun loadStep(stepNumber: Int){
        var step = stepList[stepNumber-1]

        val transaction = supportFragmentManager.beginTransaction()
        hideAll(transaction)

        if (step.isAdded) transaction.show(step)
        else transaction.add(R.id.content, step)

        transaction.commit()

        toolbar.title = "ETAPA ${stepList.indexOf(step)+1} DE ${stepList.size}"
    }
    private fun hideAll(transaction: FragmentTransaction){
        stepList.forEach {
            if(it.isAdded) transaction.hide(it)
        }
    }
    private fun isLastStep(stepNumber: Int): Boolean {
        return stepNumber == stepList.size
    }
    private fun goBackToMain(){
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finishAfterTransition()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                goPreviousStep()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
