package com.lodistudio.pead.ui.reports.reportfusion.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lodistudio.pead.R
import com.lodistudio.pead.util.AppConstants
import kotlinx.android.synthetic.main.card_weld_type.view.*

class WeldTypeAdapter(
    val contextParent: Context,
    var selected: AppConstants.WeldTypes?,
    val onclick: (AppConstants.WeldTypes?) -> Unit):
    RecyclerView.Adapter<WeldTypeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.card_weld_type, viewGroup, false)

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return AppConstants.WeldTypes.values().size
    }

    override fun onBindViewHolder(view: ViewHolder, position: Int) {
        view.bindItems(AppConstants.WeldTypes.values()[position], contextParent)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(weldType: AppConstants.WeldTypes, context: Context) {
            itemView.name.text = weldType.publicName

            val icon = ContextCompat.getDrawable(context, weldType.iconeID)
            itemView.image.setImageDrawable(icon)

            if(selected != null && weldType == selected)    itemView.background = ContextCompat.getDrawable(context, R.drawable.option_selected)
            else    itemView.background = ContextCompat.getDrawable(context, android.R.color.white)

            itemView.setOnClickListener {
                if (selected != null && selected == weldType){
                    selected = null
                }else{
                    selected = weldType
                }
                onclick(selected)
                notifyDataSetChanged()
            }

        }
    }
}