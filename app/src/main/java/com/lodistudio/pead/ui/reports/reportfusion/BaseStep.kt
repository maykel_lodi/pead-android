package com.lodistudio.pead.ui.reports.reportfusion

import android.os.Bundle
import android.os.Environment
import androidx.fragment.app.Fragment
import android.view.View

import com.lodistudio.pead.ui.newreport.NewReport
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

abstract class BaseStep : Fragment() {
    lateinit var parent: NewReport

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent = context as NewReport
    }

    abstract fun validateFields(): Boolean
    abstract fun saveFields()

    fun getFile(): File? {
        val folder = parent.getExternalFilesDir(Environment.DIRECTORY_PICTURES)


        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        var image_file: File? = null

        try {
            image_file = File.createTempFile(imageFileName, ".jpg", folder)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return image_file
    }
}
