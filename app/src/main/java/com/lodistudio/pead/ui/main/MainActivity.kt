package com.lodistudio.pead.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.ui.AppBarConfiguration
import com.google.android.gms.ads.MobileAds
import com.lodistudio.pead.R
import com.lodistudio.pead.ui.login.LoginActivity
import com.lodistudio.pead.ui.main.fragment.ReportsFragment
import com.lodistudio.pead.util.BaseActivity
import com.lodistudio.pead.util.Session
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*


class MainActivity : BaseActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MobileAds.initialize(this) { }

        setupNavigation()
    }

    fun setupNavigation(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_hamburguer)

        nav_view.setNavigationItemSelectedListener { menuItem ->
            selectMenuOption(menuItem)
            true
        }

        selectMenuOption(nav_view.menu.findItem(R.id.nav_reports))
    }

    private fun selectMenuOption(item: MenuItem){
        var fragment: Fragment

        when(item.itemId) {
            R.id.nav_reports ->{
                fragment = ReportsFragment()
            }
            R.id.nav_logout -> {
                logout()
                return
            }
            else -> {
                fragment = ReportsFragment()
            }

        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.nav_host_fragment, fragment)
            .commit()

        supportActionBar?.title = item.title

        drawer_layout.closeDrawers()
    }
    private fun logout(){
        Session().clear()
        var intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finishAfterTransition()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // The action bar home/up action should open or close the drawer.
        when (item.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }



}
