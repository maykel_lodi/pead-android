package com.lodistudio.pead.util

import android.util.Patterns

class ValidationUtils {
    companion object {
        private const val PASSWORD_MIN_LENGHT = 6
        private const val PASSWORD_MAX_LENGHT = 12


        fun isValidEmail(email: String): Boolean {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        fun isValidPassword(password: String): Boolean{
            return password.length in PASSWORD_MIN_LENGHT..PASSWORD_MAX_LENGHT
        }
    }
}

