package com.lodistudio.pead.util

import com.lodistudio.pead.R


class ValidationErrors{
    companion object {
        var requiredFieldError: String? = ""
            get() = App.resourses?.getString(R.string.ValidationErrors_requiredFieldError)

        var emailInvalidError: String? = ""
            get() = App.resourses?.getString(R.string.ValidationErrors_emailInvalidError)

        var emailAlreadyTakenError: String? = ""
            get() = App.resourses?.getString(R.string.ValidationErrors_emailAlreadyTakenError)

        var passwordInvalidLengthError: String? = ""
            get() = App.resourses?.getString(R.string.ValidationErrors_passwordInvalidLengthError)
    }
}