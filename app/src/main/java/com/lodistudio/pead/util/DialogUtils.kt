package com.lodistudio.pead.util

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.lodistudio.pead.R
import kotlinx.android.synthetic.main.dialog_utils_ok.view.*


class DialogUtils(val context: Activity) {

    companion object {
        fun okDialog(context: Context,title: String,msg: String,click: () -> Unit) {
            val view: View =
                LayoutInflater.from(context).inflate(R.layout.dialog_utils_ok, null, false)

            val builder = AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(false)
                .create()

            view.title.text = title
            view.message.text = msg
            view.button.setOnClickListener {
                builder.dismiss()
                click()
            }

            builder.setCanceledOnTouchOutside(false)
            builder.window.setBackgroundDrawable(
                ContextCompat.getDrawable(
                    context,
                    android.R.color.transparent
                )
            )
            builder.show()
        }
    }
}