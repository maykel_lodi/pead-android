package com.lodistudio.pead.util

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.net.ConnectivityManager
import com.lodistudio.pead.data.local.entity.User
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.net.InetAddress
import java.net.UnknownHostException

class Session
{
    internal var pref: SharedPreferences
    internal var editor: Editor
    internal var PRIVATE_MODE = 0

    companion object {
       const val PREF_NAME = "SessionPreferences"
        const val REPORT_CODE_PREFIX = "RF"
        const val REPORT_CODE_COUNT = "report_code_count"

        fun haveInternet(context: Context): Boolean {
            try {
                var cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                var activeNetwork = cm.activeNetworkInfo
                return activeNetwork != null
            } catch (e: UnknownHostException) {

            }
            return false
        }

    }

    init {
        pref = App.instance!!.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }





    var user: User
        get() {
            return User(
                id =                    pref.getString( "user_id",""),
                name =                  pref.getString( "user_name", null),
                phone =                 pref.getLong(   "user_phone", 0),
                email =                 pref.getString( "user_email", null),
                rg =                    pref.getLong(   "user_rg", 0),
                client_cnpj =           pref.getLong(   "user_client_cnpj", 0),
                role =                  pref.getString( "user_role", null),
                certificate_number =    pref.getString( "user_certificate_number", null),
                user_preference_id =    pref.getString( "user_preference_id", null),
                dt_created =            DateTimeFormat.forPattern("yyyy-MM-dd HH:MM:ss").parseDateTime(pref.getString("user_dt_created", DateTime.now().toString("yyyy-MM-dd HH:MM:ss"))),
                dt_modified =           DateTimeFormat.forPattern("yyyy-MM-dd HH:MM:ss").parseDateTime(pref.getString("user_dt_modified", DateTime.now().toString("yyyy-MM-dd HH:MM:ss")))
            )
        }
        set(value) {
            editor.putString("user_id",value.id)
            editor.putString("user_name", value.name)
            editor.putLong("user_phone", value.phone ?: 0)
            editor.putString("user_email", value.email)
            editor.putLong("user_rg", value.rg ?: 0)
            editor.putLong("user_client_cnpj", value.client_cnpj ?: 0)
            editor.putString("user_role", value.role)
            editor.putString("user_certificate_number", value.certificate_number)
            editor.putString("user_preference_id", value.user_preference_id)
            editor.putString("user_dt_created", value.dt_created?.toString("yyyy-MM-dd hh:MM:ss"))
            editor.putString("user_dt_modified", value.dt_modified?.toString("yyyy-MM-dd hh:MM:ss"))
            editor.commit()
        }

    fun clear(){
        editor.clear()
        editor.commit()
    }
    fun generateReportCode(): String{

        var lastcode = pref.getInt(REPORT_CODE_COUNT, 0)

        editor.putInt(REPORT_CODE_COUNT, lastcode+1)
        editor.commit()
        return "${REPORT_CODE_PREFIX}${String.format("%04d", lastcode+1)}"
    }


}