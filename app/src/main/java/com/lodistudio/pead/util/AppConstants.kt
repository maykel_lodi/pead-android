package com.lodistudio.pead.util

import android.graphics.drawable.Drawable
import com.lodistudio.pead.R
import java.text.NumberFormat

class AppConstants {

    companion object{
        const val logTitle = "Pead_Log"
        const val DEFAULT_CONNECT_TIMEOUT = 25000L
        const val DEFAULT_WRITE_TIMEOUT = 25000L
        const val DEFAULT_READ_TIMEOUT = 25000L
    }

    class STATES{

        fun getShortName(state: String): String{

            when (state){
                "Acre" ->                   return "AC"
                "Alagoas" ->                return "AL"
                "Amapá" ->                  return "AP"
                "Amazonas" ->               return "AM"
                "Bahia" ->                  return "BA"
                "Ceará" ->                  return "CE"
                "Distrito Federal" ->       return "DF"
                "Espírito Santo" ->         return "ES"
                "Goiás" ->                  return "GO"
                "Maranhão" ->               return "MA"
                "Mato Grosso" ->            return "MT"
                "Mato Grosso do Sul" ->     return "MS"
                "Minas Gerais" ->           return "MG"
                "Pará "->                   return "PA"
                "Paraíba" ->                return "PB"
                "Paraná" ->                 return "PR"
                "Pernambuco" ->             return "PE"
                "Piauí" ->                  return "PI"
                "Rio de Janeiro" ->         return "RJ"
                "Rio Grande do Norte" ->    return "RN"
                "Rio Grande do Sul" ->      return "RS"
                "Rondônia" ->               return "RO"
                "Roraima" ->                return "RR"
                "Santa Catarina" ->         return "SC"
                "São Paulo" ->              return "SP"
                "Sergipe" ->                return "SE"
                "Tocantins" ->              return "TO"
                else ->                     return ""
            }

            return ""
        }
    }

    enum class Roles(val publicName: String){
        SOLDADOR("Soldador"),
        INSPETOR("Inspetor");

        override fun toString(): String {
            return publicName
        }
    }

    enum class WeldRules(val publicName: String, val pressure_factor: Double, val heating_time_factor: Int, val freezing_time_factor: Int){
        DVS2207("DVS2207", 0.015, 10, 80),
        ISO21307 ("ISO 21307", 0.052, 11, 28);

        override fun toString(): String {
            return publicName
        }
    }

    enum class MaterialRules(val publicName: String){
        NBR15561("NBR 15561"),
        NTS194("NTS 194"),
        ISO4427("ISO 4427");

        override fun toString(): String {
            return publicName
        }
    }

    enum class ResinType(val publicName: String){
        EMPTY(""),
        PE80("PE 80"),
        PE100("PE 100");

        override fun toString(): String {
            return publicName
        }
    }


    enum class SDR(val value: Double, val pn_pe80: Double, val pn_pe100: Double){
        EMPTY(0.0,0.0,0.0),
        SDR1(7.25, 20.0, 25.0),
        SDR2(9.0, 16.0, 20.0),
        SDR3(11.0,12.5,16.0),
        SDR4(13.6,10.0,12.5),
        SDR5(17.0,8.0,10.0),
        SDR6(21.0,6.0,8.0),
        SDR7(26.0,5.0,6.0),
        SDR8(32.25,4.0,6.0);

        companion object {
            fun getPN(resTypeSel: ResinType, sdrValueSel: Double): Double{
                return if(resTypeSel == ResinType.PE80)
                    (values().filter { s -> s.value == sdrValueSel }.first()).pn_pe80
                else if(resTypeSel == ResinType.PE100)
                    (values().filter { s->s.value == sdrValueSel }.first()).pn_pe100
                else
                    0.0
            }
        }

        override fun toString(): String {
            var formatedNumber = NumberFormat.getInstance().format(value)
            return if (value != 0.0) formatedNumber else ""
        }
    }

    enum class WeldTypes(val iconeID: Int, val publicName: String){
        TUBO(R.drawable.weld_tubo, "TUBO"),
        CAP(R.drawable.weld_cap, "CAP"),
        COLARINHO(R.drawable.weld_colarinho,"COLARINHO"),
        COTOVELO45(R.drawable.weld_cotovelo45, "COTOVELO 45º"),
        COTOVELO90(R.drawable.weld_cotovelo90, "COTOVELO 90º"),
        REDUCAO(R.drawable.weld_reducao, "REDUÇÃO"),
        TEE(R.drawable.weld_tee, "TE");

        override fun toString(): String {
            return publicName
        }
    }

    enum class EquipmentManufacturers(val id: String, val publicName: String){
        EMPTY("",""),
        MCELROY("MANU001","MCELROY"),
        ROTHENBERGER("MANU002","ROTHENBERGER"),
        RITMO("MANU003","RITMO"),
        WIDOS("MANU004","WIDOS"),
        WORLDPOLY("MANU005","WORLDPOLY"),
        GF("MANU006","GF");

        companion object{
            fun getModelPistonArea(manufacturer: String, model: String): Double{
                var piston = 0.0

                var mID = values().firstOrNull { m -> m.publicName == manufacturer }?.id
                if (mID != null){
                    piston = EquipmentModels.values().firstOrNull{ m -> m.manufacturerID == mID && m.publicName == model }?.piston ?: 0.0
                }

                return piston
            }
        }

        override fun toString(): String {
            return publicName
        }
    }

    enum class EquipmentModels(val manufacturerID: String, val publicName: String,val piston: Double){
        EMPTY("","", 0.0),
        MCELROY1("MANU001","EP 250",        10.71115),
        MCELROY2("MANU001","HP 250",        10.71115),
        MCELROY3("MANU001","ROLING 412",    20.26085),
        MCELROY4("MANU001","TRACKSTAR 412", 20.26085),
        MCELROY5("MANU001","TRACKSTAR 500", 38.779525),
        MCELROY6("MANU001","TRACKSTAR 618", 38.779525),
        MCELROY7("MANU001","ROLING 824",    98.8523),
        MCELROY8("MANU001","TRACKSTAR 900", 98.8523),
        MCELROY9("MANU001","ROLING 1600",   202.73755),
        ROTHENBERGER1("MANU002", "P160",    5.53),
        ROTHENBERGER2("MANU002", "250B",    6.26),
        ROTHENBERGER3("MANU002", "315B",    6.26),
        ROTHENBERGER4("MANU002", "355B",    6.26),
        ROTHENBERGER5("MANU002", "500B2",   14.13),
        ROTHENBERGER6("MANU002", "P630",    14.13),
        ROTHENBERGER7("MANU002", "P1200",   29.84),
        RITMO1("MANU003", "BASIC 160 V1",           1.95),
        RITMO2("MANU003", "BASIC 160 V0",           2.5),
        RITMO3("MANU003", "BASIC 200 V0",           3.16),
        RITMO4("MANU003", "BASIC 250 V1",           5.89),
        RITMO5("MANU003", "BASIC 315V0",            6.67),
        RITMO6("MANU003", "BASIC 355 V0",           14.13),
        RITMO7("MANU003", "DELTA DRAGON 160V1",     16.47),
        RITMO8("MANU003", "DELTA 250B",             5.89),
        RITMO9("MANU003", "DELTA 315B",             6.67),
        RITMO10("MANU003", "DELTA 355B",            14.13),
        RITMO11("MANU003", "DELTA 500 V1",          22.38),
        RITMO12("MANU003", "DELTA 630",             43.98),
        RITMO13("MANU003", "DELTA 630 V1",          20.42),
        RITMO14("MANU003", "DELTA 800",             23.55),
        RITMO15("MANU003", "DELTA 630 ALL TERRAIN", 56.91),
        WIDOS1("MANU004", "4911",   5.9),
        WIDOS2("MANU004", "6113",   17.3),
        WORLDPOLY1("MANU005", "WHD 250", 11.0),
        WORLDPOLY2("MANU005", "WHD 315", 20.0),
        WORLDPOLY3("MANU005", "WHD 450", 22.36),
        WORLDPOLY4("MANU005", "WHD 630", 23.06),
        WORLDPOLY5("MANU005", "WHD 800", 20.4),
        WORLDPOLY6("MANU005", "WHD 1000",25.12),
        WORLDPOLY7("MANU005", "WHD 1200",32.97),
        GF1("MANU006", "GF160", 3.53),
        GF2("MANU006", "GF250", 5.1),
        GF3("MANU006", "GF315", 5.1),
        GF4("MANU006", "GF630", 23.56),
        GF5("MANU006", "GF800", 29.84),
        GF6("MANU006", "GF1000",29.84);

        companion object {
            fun getModelsByManufacturerID(manufacturerID: String): List<EquipmentModels> {
                var list = ArrayList<EquipmentModels>()
                list.add(EMPTY)
                list.addAll(EquipmentModels.values().filter { e -> e.manufacturerID == manufacturerID })
                return list
            }
        }

        override fun toString(): String {
            return publicName
        }
    }
}