package com.lodistudio.pead.util

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import com.lodistudio.pead.R
import kotlinx.android.synthetic.main.progressbar_basic.*
import org.joda.time.DateTime

open class BaseActivity : AppCompatActivity(), LifecycleOwner {
    lateinit var progressBar: AlertDialog


    override fun setContentView(layoutResID: Int) {
        var baseActivityWithProgress: FrameLayout =
            layoutInflater.inflate(R.layout.progressbar_basic, null) as FrameLayout
        var activityContainer: FrameLayout = baseActivityWithProgress.findViewById(R.id.frame_activity)

        layoutInflater.inflate(layoutResID, activityContainer, true)
        super.setContentView(baseActivityWithProgress)
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            View.SYSTEM_UI_FLAG_FULLSCREEN
            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        }
    }

    // ESCONDER TECLADO QUANDO CLICAR FORA  >> Melhor Jeito "Works like a charm"<<
    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    fun showLoading(text: String? = null) {
        if (progress.visibility == View.GONE) progress.visibility = View.VISIBLE
        if (progressView.visibility == View.GONE) progressView.visibility = View.VISIBLE
        if (!text.isNullOrEmpty()) progress_text.text = text
            else progress_text.text = "Carregando..."
    }
    fun hideLoading() {
        if (progress.visibility == View.VISIBLE) progress.visibility = View.GONE
        if (progressView.visibility == View.VISIBLE) progressView.visibility = View.GONE
    }
    fun toast(text: String){
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }
    fun log(message: String){
        Log.d(AppConstants.logTitle, "${DateTime.now()} - ${this.javaClass}: $message")
    }
}