package com.lodistudio.pead.util

import com.lodistudio.pead.BuildConfig
import com.lodistudio.pead.data.remote.interceptor.TimeoutInterceptor
import com.lodistudio.pead.data.remote.service.*
import com.lodistudio.pead.util.AppConstants.Companion.DEFAULT_CONNECT_TIMEOUT
import com.lodistudio.pead.util.AppConstants.Companion.DEFAULT_READ_TIMEOUT
import com.lodistudio.pead.util.AppConstants.Companion.DEFAULT_WRITE_TIMEOUT
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient {
    val BASE_URL = BuildConfig.BASE_URL

    val logging = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)

    private val client = OkHttpClient.Builder()
        .addNetworkInterceptor(logging)
        .connectTimeout(DEFAULT_CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
        .writeTimeout(DEFAULT_WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
        .readTimeout(DEFAULT_READ_TIMEOUT, TimeUnit.MILLISECONDS)
        .addInterceptor(TimeoutInterceptor())
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()


    fun WeatherAPIService() = retrofit.create(WeatherAPIService::class.java)
    fun ReportFusionService() = retrofit.create(ReportFusionService::class.java)
}