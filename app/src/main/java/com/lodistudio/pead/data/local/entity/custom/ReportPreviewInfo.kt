package com.lodistudio.pead.data.local.entity.custom

import org.joda.time.DateTime

data class ReportPreviewInfo(
    val id: String? = null,
    val code: String? = null,
    val description: String? = null,
    val aborted_report_reason: String? = null,
    val dt_start: DateTime? = null,
    val dt_finished: DateTime? = null,
    val dt_aborted: DateTime? = null
)