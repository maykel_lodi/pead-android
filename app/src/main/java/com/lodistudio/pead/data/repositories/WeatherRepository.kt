package com.lodistudio.pead.data.repositories

import android.content.Context
import com.lodistudio.pead.data.local.entity.User
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lodistudio.pead.data.local.AppDatabase
import com.lodistudio.pead.data.local.entity.UserPreference
import com.lodistudio.pead.data.remote.response.CityResponse
import com.lodistudio.pead.data.remote.response.RegisterCityResponse
import com.lodistudio.pead.util.AppConstants
import com.lodistudio.pead.util.RetrofitClient
import com.lodistudio.pead.util.SingleLiveEvent
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class WeatherRepository{
    companion object{
        const val hgToken = "1a3efc8f"

        @Volatile private var INSTANCE: WeatherRepository? = null
        lateinit var database: AppDatabase

        fun getInstance(context: Context): WeatherRepository? {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE ?: WeatherRepository().also { INSTANCE = it }
                }
            }
            database = AppDatabase.getInstance(context)!!

            return INSTANCE
        }
    }

    fun getWheather(latitude: Double, longitude: Double): LiveData<CityResponse> {
        val wheaterInfo = MutableLiveData<CityResponse>()

        RetrofitClient()
            .WeatherAPIService()
            .getCity(hgToken, latitude.toString(), longitude.toString())
            .enqueue(object : Callback<CityResponse?> {
                override fun onResponse(call: Call<CityResponse?>?, response: Response<CityResponse?>?) {
                    response?.body()?.let {
                        wheaterInfo.postValue(it)
                    }
                }
                override fun onFailure(call: Call<CityResponse?>?, t: Throwable?) {
                    wheaterInfo.postValue(null)
                }
            })
        return wheaterInfo
    }
}