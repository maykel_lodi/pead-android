package com.lodistudio.pead.data.remote.service
import com.lodistudio.pead.data.remote.response.CityResponse
import com.lodistudio.pead.data.remote.response.RegisterCityResponse
import retrofit2.Call
import retrofit2.http.*

interface WeatherAPIService {

    @Headers("CONNECT_TIMEOUT:10000", "READ_TIMEOUT:10000", "WRITE_TIMEOUT:10000")
    @GET("https://api.hgbrasil.com/weather")
    fun getCity(@Query("key") token: String,
                @Query("lat") lat: String,
                @Query("long") long: String,
                @Query("user_ip") user_ip: String = "remote"): Call<CityResponse>

}