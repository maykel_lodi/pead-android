package com.lodistudio.pead.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.lodistudio.pead.data.local.entity.Role

@Dao
interface RoleDAO{

    @Query("SELECT * FROM Role")
    fun getAll(): LiveData<List<Role>>

    @Query("DELETE FROM Role")
    fun deleteAll()

    @Insert(onConflict = REPLACE)
    fun insert(role: Role)

    @Insert(onConflict = REPLACE)
    fun insertAll(roles: List<Role>)

    @Update
    fun update(role: Role)

    @Delete
    fun delete(role: Role)
}