package com.lodistudio.pead.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.DateTime
import java.util.*

@Entity
data class Role(
    @ColumnInfo(name = "id", index = true)
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    var description: String? = null,
    var dt_created: DateTime? = null,
    var dt_modified: DateTime? = null
){
    fun populateData(): List<Role>{
         return listOf(
            Role(description = "SOLDADOR"),
            Role(description = "INSPETOR")
        )
    }

    override fun toString(): String {
        return "$description"
    }

}