package com.lodistudio.pead.data.repositories

import android.content.Context
import com.lodistudio.pead.data.local.entity.User
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import com.lodistudio.pead.data.local.AppDatabase
import com.lodistudio.pead.data.local.entity.Role
import com.lodistudio.pead.data.local.entity.UserPreference
import com.lodistudio.pead.util.AppConstants
import org.joda.time.DateTime
import org.joda.time.DateTimeZone


class RoleRepository{
    companion object{
        @Volatile private var INSTANCE: RoleRepository? = null
        lateinit var database: AppDatabase

        fun getInstance(context: Context): RoleRepository? {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE ?: RoleRepository().also { INSTANCE = it }
                }
            }
            database = AppDatabase.getInstance(context)!!

            return INSTANCE
        }
    }

    fun getAll(): LiveData<List<Role>> {
        return database.roleDAO().getAll()
    }

}