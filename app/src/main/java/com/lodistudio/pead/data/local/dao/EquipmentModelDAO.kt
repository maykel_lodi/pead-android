package com.lodistudio.pead.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.lodistudio.pead.data.local.entity.EquipmentModel

@Dao
interface EquipmentModelDAO{

    @Query("SELECT * FROM EquipmentModel")
    fun getAll(): LiveData<List<EquipmentModel>?>

    @Query("SELECT * FROM EquipmentModel WHERE equipment_manufacturer_id = :manufacturerID")
    fun getAllByManufacturerID(manufacturerID: String): LiveData<List<EquipmentModel>?>

    @Query("DELETE FROM EquipmentModel")
    fun deleteAll()

    @Insert(onConflict = REPLACE)
    fun insert(model: EquipmentModel)

    @Insert(onConflict = REPLACE)
    fun insertAll(models: List<EquipmentModel>)

    @Update
    fun update(model: EquipmentModel)

    @Delete
    fun delete(model: EquipmentModel)
}