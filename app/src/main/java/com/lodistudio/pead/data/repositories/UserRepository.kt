package com.lodistudio.pead.data.repositories

import android.content.Context
import com.lodistudio.pead.data.local.entity.User
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import com.lodistudio.pead.data.local.AppDatabase
import com.lodistudio.pead.data.local.entity.UserPreference
import com.lodistudio.pead.util.AppConstants
import com.lodistudio.pead.util.SingleLiveEvent
import org.joda.time.DateTime
import org.joda.time.DateTimeZone


class UserRepository{
    companion object{
        @Volatile private var INSTANCE: UserRepository? = null
        lateinit var database: AppDatabase

        fun getInstance(context: Context): UserRepository? {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE ?: UserRepository().also { INSTANCE = it }
                }
            }
            database = AppDatabase.getInstance(context)!!

            return INSTANCE
        }
    }

    fun insert(user: User){
        var now = DateTime.now(DateTimeZone.getDefault())

        var userPref = UserPreference(
            dt_created = now,
            dt_modified = now
        )
        InsertUserPreferenceAsync().execute(userPref)

        user.dt_created = now
        user.dt_modified = now
        user.user_preference_id = userPref.id
        InsertUserAsync().execute(user)
        //Send to Backend
        log("Usuário ${user.name} foi salvo.")
    }
    fun getUserByEmail(email: String): LiveData<User?> {
        return database.userDAO().getByEmail(email)
    }

    inner class InsertUserAsync : AsyncTask<User, Void, Void>() {
        override fun doInBackground(vararg params: User): Void? {
            database?.userDAO()?.insert(params[0])
            return null
        }
    }
    inner class InsertUserPreferenceAsync : AsyncTask<UserPreference, Void, Void>() {
        override fun doInBackground(vararg params: UserPreference): Void? {
            database?.userPreferenceDAO()?.insert(params[0])
            return null
        }
    }

    private fun log(message: String){
        Log.d(AppConstants.logTitle, "${DateTime.now()} - UserRepository: $message")
    }
}