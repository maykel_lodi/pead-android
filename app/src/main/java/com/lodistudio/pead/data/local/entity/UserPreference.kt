package com.lodistudio.pead.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import org.joda.time.DateTime
import java.util.*

@Entity(foreignKeys = [
        ForeignKey(entity = EquipmentManufacturer::class,
            parentColumns = ["id"],
            childColumns = ["equipment_manufacturer_id"]),
        ForeignKey(entity = EquipmentModel::class,
            parentColumns = ["id"],
            childColumns = ["equipment_model_id"])
        ])
data class UserPreference(
    @ColumnInfo(index = true)
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "equipment_manufacturer_id", index = true)
    var equipment_manufacturer_id: String? = null,
    @ColumnInfo(name = "equipment_model_id", index = true)
    var equipment_model_id: String? = null,
    var dt_created: DateTime? = null,
    var dt_modified: DateTime? = null
)