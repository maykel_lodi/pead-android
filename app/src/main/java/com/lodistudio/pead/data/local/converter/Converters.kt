package com.lodistudio.pead.data.local.converter

import androidx.room.TypeConverter
import org.joda.time.DateTime
import org.joda.time.DateTimeZone


class Converters {

    @TypeConverter
    fun fromTimestamp(value: Long?): DateTime? {
        return if (value == null) null else DateTime(value, DateTimeZone.UTC)
    }

    @TypeConverter
    fun dateTimeToTimestamp(date: DateTime?): Long? {
        return (if (date == null) null else date!!.millis)
    }

}