package com.lodistudio.pead.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import org.joda.time.DateTime
import java.util.*

@Entity(foreignKeys = [
    ForeignKey(entity = EvidenceType::class,
        parentColumns = ["id"],
        childColumns = ["evidence_type_id"]),
    ForeignKey(entity = ReportFusion::class,
        parentColumns = ["id"],
        childColumns = ["report_fusion_id"])
])
data class ReportEvidence(
    @ColumnInfo(index = true)
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    var evidence_type_id: String? = null,
    var report_fusion_id: String? = null,
    var file_name: String? = null,
    var file_extension: String? = null,
    var file_path: String? = null,
    var dt_created: DateTime? = null,
    var dt_modified: DateTime? = null
)