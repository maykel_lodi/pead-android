package com.lodistudio.pead.data.local.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.lodistudio.pead.data.local.entity.User
import com.lodistudio.pead.data.local.entity.UserPreference

@Dao
interface UserPreferenceDAO{

    @Query("SELECT * FROM UserPreference")
    fun getAll(): List<UserPreference>

    @Insert(onConflict = REPLACE)
    fun insert(user: UserPreference)

    @Update
    fun update(user: UserPreference)

    @Delete
    fun delete(user: UserPreference)
}