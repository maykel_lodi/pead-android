package com.lodistudio.pead.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.lodistudio.pead.data.local.entity.User

@Dao
interface UserDAO{

    @Query("SELECT * FROM User")
    fun getAll(): List<User>

    @Query("SELECT * FROM User WHERE email = :email")
    fun getByEmail(email: String): LiveData<User?>

    @Insert(onConflict = REPLACE)
    fun insert(user: User)

    @Insert(onConflict = REPLACE)
    fun insertAll(users: List<User>)

    @Update
    fun update(user: User)

    @Delete
    fun delete(user: User)
}