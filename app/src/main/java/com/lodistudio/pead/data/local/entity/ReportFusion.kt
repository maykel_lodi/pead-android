package com.lodistudio.pead.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import org.joda.time.DateTime
import java.util.*

@Entity(foreignKeys = [
    ForeignKey(entity = User::class,
        parentColumns = ["id"],
        childColumns = ["user_id"])
    ])
data class ReportFusion(
    @ColumnInfo(index = true)
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    var code: String? = null,
    @ColumnInfo(name = "user_id", index = true)
    var user_id: String? = null,
    var user_name: String? = null,
    var user_latitude: Double? = null,
    var user_longitude: Double? = null,
    var dt_start: DateTime? = null,
    var description: String? = null,
    var climatic_description: String? = null,
    var climatic_humidity: Double? = null,
    var climatic_temperature: Double? = null,
    var material_1_type: String? = null,
    var material_2_type: String? = null,
    var material_1_manufacturer: String? = null,
    var material_2_manufacturer: String? = null,
    var material_1_external_diameter: Int? = null,
    var material_2_external_diameter: Int? = null,
    var material_1_resin_type: String? = null,
    var material_2_resin_type: String? = null,
    var material_1_pn: Double? = null,
    var material_2_pn: Double? = null,
    var material_1_sdr: Double? = null,
    var material_2_sdr: Double? = null,
    var material_1_lote_number: String? = null,
    var material_2_lote_number: String? = null,
    var material_1_lote_number_evidence: String? = null,
    var material_2_lote_number_evidence: String? = null,
    var material_1_rule: String? = null,
    var material_2_rule: String? = null,
    var equipment_manufacturer_name: String? = null,
    var equipment_model_name: String? = null,
    var equipment_serie_number: String? = null,
    var equipment_serie_number_evidence: String? = null,
    var equipment_dt_checked: DateTime? = null,
    var equipment_dt_checked_evidence: String? = null,
    var weld_pressure: Double? = null,
    var weld_pipe_wall: Double? = null,
    var weld_drag_pressure: Int? = null,
    var weld_initial_cord_height: Double? = null,
    var weld_heating_plate_temperature: Double? = null,
    var weld_heating_plate_temperature_evidence: String? = null,
    var weld_heating_total_time: Double? = null,
    var weld_cooling_total_time: Double? = null,
    var weld_plate_out_time: Double? = null,
    var weld_final_width_strings_side1: Double? = null,
    var weld_final_width_strings_side2: Double? = null,
    var weld_dt_process_start: DateTime? = null,
    var weld_dt_process_end: DateTime? = null,
    var equipment_condition: String? = null,
    var report_weld_approved: Boolean? = false,
    var dt_aborted: DateTime? = null,
    var aborted_report_reason: String? = null,
    var dt_finished: DateTime? = null,
    var dt_created: DateTime? = null,
    var dt_modified: DateTime? = null

)