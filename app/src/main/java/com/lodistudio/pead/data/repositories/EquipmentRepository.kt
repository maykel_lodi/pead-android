package com.lodistudio.pead.data.repositories

import android.content.Context
import com.lodistudio.pead.data.local.entity.User
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import com.lodistudio.pead.data.local.AppDatabase
import com.lodistudio.pead.data.local.entity.EquipmentManufacturer
import com.lodistudio.pead.data.local.entity.EquipmentModel
import com.lodistudio.pead.data.local.entity.UserPreference
import com.lodistudio.pead.util.AppConstants
import com.lodistudio.pead.util.SingleLiveEvent
import org.joda.time.DateTime
import org.joda.time.DateTimeZone


class EquipmentRepository{
    companion object{
        @Volatile private var INSTANCE: EquipmentRepository? = null
        lateinit var database: AppDatabase

        fun getInstance(context: Context): EquipmentRepository? {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE ?: EquipmentRepository().also { INSTANCE = it }
                }
            }
            database = AppDatabase.getInstance(context)!!

            return INSTANCE
        }
    }

    fun insertManufacturer(manufacturer: EquipmentManufacturer){
        var now = DateTime.now(DateTimeZone.getDefault())

        manufacturer.dt_created = now
        manufacturer.dt_modified = now
        InsertManufacturerAsync().execute(manufacturer)
        //Send to Backend
        log("Equipment Manufacturer ${manufacturer.description} foi salvo.")
    }
    fun insertModel(model: EquipmentModel){
        var now = DateTime.now(DateTimeZone.getDefault())

        model.dt_created = now
        model.dt_modified = now
        InsertModelAsync().execute(model)
        //Send to Backend
        log("Equipment Model ${model.description} foi salvo.")
    }
    fun getAllManufacturers(): LiveData<List<EquipmentManufacturer>?> {
        return database.equipmentManufacturerDAO().getAll()
    }
    fun getAllModelsByManufacturerID(manufacturerID: String): LiveData<List<EquipmentModel>?> {
        return database.equipmentModelDAO().getAllByManufacturerID(manufacturerID)
    }

    inner class InsertManufacturerAsync : AsyncTask<EquipmentManufacturer, Void, Void>() {
        override fun doInBackground(vararg params: EquipmentManufacturer): Void? {
            database?.equipmentManufacturerDAO()?.insert(params[0])
            return null
        }
    }
    inner class InsertModelAsync : AsyncTask<EquipmentModel, Void, Void>() {
        override fun doInBackground(vararg params: EquipmentModel): Void? {
            database?.equipmentModelDAO()?.insert(params[0])
            return null
        }
    }

    private fun log(message: String){
        Log.d(AppConstants.logTitle, "${DateTime.now()} - UserRepository: $message")
    }
}