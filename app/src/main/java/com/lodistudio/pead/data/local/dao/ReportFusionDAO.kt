package com.lodistudio.pead.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.lodistudio.pead.data.local.entity.ReportFusion
import com.lodistudio.pead.data.local.entity.User
import com.lodistudio.pead.data.local.entity.custom.ReportPreviewInfo

@Dao
interface ReportFusionDAO{

    @Query("SELECT * FROM ReportFusion")
    fun getAll(): List<ReportFusion>

    @Query("SELECT id, code, description, aborted_report_reason, dt_start, dt_finished, dt_aborted FROM ReportFusion")
    fun getAllPreview(): LiveData<List<ReportPreviewInfo>>

    @Query("SELECT * FROM ReportFusion WHERE id = :id")
    fun getByID(id: String): LiveData<ReportFusion>

    @Insert(onConflict = REPLACE)
    fun insert(user: ReportFusion)

    @Update
    fun update(user: ReportFusion)

    @Delete
    fun delete(user: ReportFusion)
}