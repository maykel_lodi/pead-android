package com.lodistudio.pead.data.local.entity

import androidx.room.*
import org.joda.time.DateTime
import java.util.*

@Entity
data class User(
    @ColumnInfo(index = true)
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    var name: String? = null,
    var phone: Long? = null,
    var email: String? = null,
    var password: String? = null,
    var rg: Long? = null,
    var client_cnpj: Long? = null,
    var role: String? = null,
    var certificate_number: String? = null,
    var user_preference_id: String? = null,
    var dt_created: DateTime? = null,
    var dt_modified: DateTime? = null
) {
    override fun toString(): String {
        return "User(id='$id', name=$name, phone=$phone, email=$email, password=$password, rg=$rg, client_cnpj=$client_cnpj, role_id=$role, certificate_number=$certificate_number, user_preference_id=$user_preference_id, dt_created=$dt_created, dt_modified=$dt_modified)"
    }

    fun populateData(): List<User>{
        return listOf(
            User(
                name = "Maykel Lodi",
                email = "maykel.lodi@gmail.com",
                password = "123456"
            )
        )
    }
}
