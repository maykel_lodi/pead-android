package com.lodistudio.pead.data.remote.response


class RegisterCityResponse (
        val status: String? = null,
        val locales: List<Long>? = emptyList()
    ){
}
