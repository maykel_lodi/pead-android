package com.lodistudio.pead.data.remote.response

import org.joda.time.DateTime


class WeatherInfoResponse (
        val temperature: Double? = null,
        val wind_direction: String? = null,
        val wind_velocity: Double? = null,
        val humidity: Double? = null,
        val condition: String? = null,
        val pressure: Double? = null,
        val icon: String? = null,
        val sensation: Int? = null,
        val date: String? = null
    ){
}
