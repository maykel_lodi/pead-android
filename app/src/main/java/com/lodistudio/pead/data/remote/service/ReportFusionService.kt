package com.lodistudio.pead.data.remote.service

import com.lodistudio.pead.data.remote.response.Result
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ReportFusionService {

    @Multipart
    @POST("Report/sendReportEmail")
    fun sendReportEmail(@PartMap params: HashMap<String,RequestBody>,  @Part file : MultipartBody.Part ): Call<Result<Boolean>>

}