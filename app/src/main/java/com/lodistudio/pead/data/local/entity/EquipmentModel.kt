package com.lodistudio.pead.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import org.joda.time.DateTime
import java.util.*

@Entity(foreignKeys = [
    ForeignKey(entity = EquipmentManufacturer::class,
        parentColumns = ["id"],
        childColumns = ["equipment_manufacturer_id"])
])
data class EquipmentModel(
    @ColumnInfo(index = true)
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "equipment_manufacturer_id", index = true)
    var equipment_manufacturer_id: String? = null,
    var description: String? = null,
    var piston_area: Double? = null,
    var dt_created: DateTime? = null,
    var dt_modified: DateTime? = null
)