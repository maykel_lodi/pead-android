package com.lodistudio.pead.data.repositories

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lodistudio.pead.data.local.AppDatabase
import com.lodistudio.pead.data.local.entity.ReportFusion
import com.lodistudio.pead.data.local.entity.custom.ReportPreviewInfo
import com.lodistudio.pead.data.remote.response.Result
import com.lodistudio.pead.util.AppConstants
import com.lodistudio.pead.util.RetrofitClient
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ReportFusionRepository{
    companion object{
        @Volatile private var INSTANCE: ReportFusionRepository? = null
        lateinit var database: AppDatabase

        fun getInstance(context: Context): ReportFusionRepository? {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE ?: ReportFusionRepository().also { INSTANCE = it }
                }
            }
            database = AppDatabase.getInstance(context)!!

            return INSTANCE
        }
    }

    fun sendReportEmail(request: HashMap<String, RequestBody>, file : MultipartBody.Part): LiveData<Result<Boolean>>{
        var sent = MutableLiveData<Result<Boolean>>()

        RetrofitClient()
            .ReportFusionService()
            .sendReportEmail(request, file)
            .enqueue(object : Callback<Result<Boolean>?> {
                override fun onResponse(call: Call<Result<Boolean>?>, result: Response<Result<Boolean>?>) {
                    result?.body()?.let {
                        sent.postValue(it)
                    }
                }
                override fun onFailure(call: Call<Result<Boolean>?>, t: Throwable?) {
                    sent.postValue(null)
                }
            })


        return sent
    }

    fun insert(report: ReportFusion){
        var now = DateTime.now(DateTimeZone.getDefault())


        report.dt_created = now
        report.dt_modified = now

        InsertReportFusionAsync().execute(report)
        log("Relatório ${report.id} foi salvo.")
    }

    fun getAllPreview(): LiveData<List<ReportPreviewInfo>>{
        return database?.reportFusionDAO()?.getAllPreview()
    }

    fun getReportByID(id: String): LiveData<ReportFusion>{
        return database?.reportFusionDAO()?.getByID(id)
    }

    inner class InsertReportFusionAsync : AsyncTask<ReportFusion, Void, Void>() {
        override fun doInBackground(vararg params: ReportFusion): Void? {
            database?.reportFusionDAO()?.insert(params[0])
            return null
        }
    }
    private fun log(message: String){
        Log.d(AppConstants.logTitle, "${DateTime.now()} - UserRepository: $message")
    }
}