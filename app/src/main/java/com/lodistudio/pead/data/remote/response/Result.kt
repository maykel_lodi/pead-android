package com.lodistudio.pead.data.remote.response


class Result<T> (
        val data: T,
        val mensagens: List<String>? = emptyList(),
        val isSuccess: Boolean
)

