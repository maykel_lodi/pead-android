package com.lodistudio.pead.data.model

import org.joda.time.DateTime

data class UserModel(
    val id: String? = null,
    var name: String? = null,
    var phone: Long? = null,
    var email: String? = null,
    var password: String? = null,
    var rg: Long? = null,
    var client_cnpj: Long? = null,
    var role_id: String? = null,
    var certificate_number: String? = null,
    var user_preference_id: String? = null,
    var dt_created: DateTime? = null,
    var dt_modified: DateTime? = null
) {
    override fun toString(): String {
        return "User(id='$id', name=$name, phone=$phone, email=$email, password=$password, rg=$rg, client_cnpj=$client_cnpj, role_id=$role_id, certificate_number=$certificate_number, user_preference_id=$user_preference_id, dt_created=$dt_created, dt_modified=$dt_modified)"
    }
}