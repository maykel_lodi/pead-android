package com.lodistudio.pead.data.remote.response


class CityResponse (
        val valid_key: Boolean? = null,
        val results: WeatherResult? = null
    ){
}

class WeatherResult(
       val temp: Double? = null,
       val description: String? = null,
       val humidity: Double
){}

