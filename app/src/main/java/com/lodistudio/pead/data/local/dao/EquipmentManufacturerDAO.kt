package com.lodistudio.pead.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.lodistudio.pead.data.local.entity.EquipmentManufacturer

@Dao
interface EquipmentManufacturerDAO{

    @Query("SELECT * FROM EquipmentManufacturer")
    fun getAll(): LiveData<List<EquipmentManufacturer>?>

    @Query("DELETE FROM EquipmentManufacturer")
    fun deleteAll()

    @Insert(onConflict = REPLACE)
    fun insert(manufacturer: EquipmentManufacturer)

    @Insert(onConflict = REPLACE)
    fun insertAll(manufacturers: List<EquipmentManufacturer>)

    @Update
    fun update(manufacturer: EquipmentManufacturer)

    @Delete
    fun delete(manufacturer: EquipmentManufacturer)
}