package com.lodistudio.pead.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.DateTime
import java.util.*

@Entity
data class MaterialType(
    @ColumnInfo(index = true)
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    val description: String? = null,
    var dt_created: DateTime? = null,
    var dt_modified: DateTime? = null
)