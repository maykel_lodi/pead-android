package com.lodistudio.pead.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.Room
import androidx.room.TypeConverters
import com.lodistudio.pead.data.local.converter.Converters
import com.lodistudio.pead.data.local.entity.*
import android.os.AsyncTask.execute
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.annotation.NonNull
import android.icu.lang.UCharacter.GraphemeClusterBreak.V
import android.os.AsyncTask
import android.icu.lang.UCharacter.GraphemeClusterBreak.V
import android.R.attr.name
import com.lodistudio.pead.data.local.dao.*
import java.util.concurrent.Executors


@Database(entities = [  User::class,
                        Role::class,
                        UserPreference::class,
                        ReportFusion::class,
                        WeldType::class,
                        ProcedureType::class,
                        MaterialType::class,
                        EvidenceType::class,
                        EquipmentModel::class,
                        EquipmentManufacturer::class,
                        EquipmentCondition::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {

    private var appDatabase: AppDatabase? = null

    abstract fun userDAO(): UserDAO
    abstract fun userPreferenceDAO(): UserPreferenceDAO
    abstract fun roleDAO(): RoleDAO
    abstract fun equipmentManufacturerDAO(): EquipmentManufacturerDAO
    abstract fun equipmentModelDAO(): EquipmentModelDAO
    abstract fun reportFusionDAO(): ReportFusionDAO

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java, "pead_database1.db"
                    )
                        .addCallback(object : RoomDatabase.Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                Executors.newSingleThreadScheduledExecutor().execute(Runnable {
                                    getInstance(context)?.userDAO()?.insertAll(User().populateData())
                                })
                            }
                        })
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }


}
